package me.aecsocket.unifiedframework.npc;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/** {@link NPCManager}'s event {@link Listener}. */
public class EventHandle implements Listener {
    private NPCManager manager;

    public EventHandle(NPCManager manager) {
        this.manager = manager;
    }

    public NPCManager getManager() { return manager; }

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
        Entity entity = event.getRightClicked();
        NPC npc = manager.getNPC(entity);
        if (npc != null)
            npc.onClick(event);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        NPC npc = manager.getNPC(entity);
        if (npc != null && event.getCause() != EntityDamageEvent.DamageCause.VOID)
            npc.onDamage(event);
    }
}
