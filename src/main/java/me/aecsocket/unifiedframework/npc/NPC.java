package me.aecsocket.unifiedframework.npc;

import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/** A wrapper around {@link Entity} with right-click and damage detection. */
public abstract class NPC {
    private Entity handle;

    public NPC(Entity handle) {
        this.handle = handle;
    }

    public Entity getHandle() { return handle; }
    public void setHandle(Entity handle) { this.handle = handle; }

    /** When the handle {@link Entity} is right-clicked.
     * @param event The {@link PlayerInteractEntityEvent}.
     */
    public void onClick(PlayerInteractEntityEvent event) {}

    /** When the handle {@link Entity} is damaged.
     * @param event The {@link EntityDamageEvent}.
     */
    public void onDamage(EntityDamageEvent event) {}
}
