package me.aecsocket.unifiedframework.npc;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/** Allows adding custom entity {@link NPC}s which detect right-clicks. */
public class NPCManager {
    private List<NPC> npcs;

    public NPCManager(Plugin plugin) {
        this.npcs = new ArrayList<>();
        Bukkit.getPluginManager().registerEvents(new EventHandle(this), plugin);
    }

    /** Gets a {@link List} of all {@link NPC}s.
     * @return A {@link List} of all {@link NPC}s.
     */
    public List<NPC> getNPCs() { return npcs; }

    /** Gets an {@link NPC} from an {@link Entity}, or null if there is none.
     * @param entity The {@link Entity}.
     * @return The {@link NPC} or null if there is no {@link Entity} handle.
     */
    public NPC getNPC(Entity entity) { return npcs.stream().filter(r -> r.getHandle() == entity).findFirst().orElse(null); }

    /** Adds an {@link NPC}.
     * @param npc The {@link NPC}.
     */
    public void registerNPC(NPC npc) { npcs.add(npc); }

    /** Removes an {@link NPC}.
     * @param npc The {@link NPC}.
     */
    public void unregisterNPC(NPC npc) { npcs.remove(npc); }
}
