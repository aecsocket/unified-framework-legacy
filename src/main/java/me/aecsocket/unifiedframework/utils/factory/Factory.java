package me.aecsocket.unifiedframework.utils.factory;

public interface Factory<T> {
    T create(FactoryContext context);
}
