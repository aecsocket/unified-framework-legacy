package me.aecsocket.unifiedframework.utils.factory;

import me.aecsocket.unifiedframework.utils.InitMap;

import java.util.Optional;

public class FactoryMap<F extends Factory<T>, T> extends InitMap<String, F> {
    private F defaultFactory;

    public F getDefaultFactory() { return defaultFactory; }
    public void setDefaultFactory(F defaultFactory) { this.defaultFactory = defaultFactory; }
    public FactoryMap<F, T> defaultFactory(F defaultFactory) { this.defaultFactory = defaultFactory; return this; }

    @Override
    public FactoryMap<F, T> add(String key, F value) { return (FactoryMap<F, T>)super.add(key, value); }

    public Optional<T> create(String type, FactoryContext context) {
        if (containsKey(type))
            return Optional.of(get(type).create(context));
        if (defaultFactory == null)
            return Optional.empty();
        return Optional.of(defaultFactory.create(context));
    }
}
