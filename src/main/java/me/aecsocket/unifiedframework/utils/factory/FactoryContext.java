package me.aecsocket.unifiedframework.utils.factory;

import java.util.HashMap;
import java.util.Map;

public class FactoryContext {
    private Map<String, Object> data = new HashMap<>();

    public FactoryContext add(String key, Object value) { data.put(key, value); return this; }
    public Map<String, Object> get() { return new HashMap<>(data); }
    public Object get(String key) { return data.get(key); }
    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> type) { return (T)data.get(key); }
}
