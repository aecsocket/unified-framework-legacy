package me.aecsocket.unifiedframework.utils;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class NamedPlayerProfile implements PlayerProfile {
    private String name;

    public NamedPlayerProfile(String name) {
        this.name = name;
    }

    @Nullable
    @Override public String getName() { return name; }
    @NotNull
    @Override public String setName(String name) { String old = this.name; this.name = name; return old; }

    @Override public @Nullable UUID getId() { return null; }
    @Override public @Nullable UUID setId(@Nullable UUID uuid) { return null; }
    @Override public @NotNull Set<ProfileProperty> getProperties() { return Collections.emptySet(); }
    @Override public boolean hasProperty(@Nullable String s) { return false; }
    @Override public void setProperty(@NotNull ProfileProperty profileProperty) {}
    @Override public void setProperties(@NotNull Collection<ProfileProperty> collection) {}
    @Override public boolean removeProperty(@Nullable String s) { return false; }
    @Override public void clearProperties() {}
    @Override public boolean isComplete() { return false; }
    @Override public boolean completeFromCache() { return false; }
    public boolean completeFromCache(boolean b) { return false; }
    public boolean completeFromCache(boolean b, boolean b1) { return false; }
    @Override public boolean complete() { return false; }
    @Override public boolean complete(boolean b) { return false; }
    public boolean complete(boolean b, boolean b1) { return false; }
}
