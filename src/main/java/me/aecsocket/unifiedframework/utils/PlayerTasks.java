package me.aecsocket.unifiedframework.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Handles tasks linked to a {@link Player}, which get cancelled when they die or disconnect. */
public final class PlayerTasks {
    private PlayerTasks() {}

    private static final Map<Player, List<Integer>> playerTasks = new HashMap<>();

    /** Gets a {@link Map} of each {@link Player} mapped to a {@link List} of their tasks.
     * @return A {@link Map} of each {@link Player} mapped to a {@link List} of their tasks.
     */
    public static Map<Player, List<Integer>> getPlayerTasks() { return new HashMap<>(playerTasks); }

    /** Gets a {@link List} of all {@link org.bukkit.scheduler.BukkitScheduler} tasks a {@link Player} has.
     * @param player The {@link Player}.
     * @return A {@link List} of all {@link org.bukkit.scheduler.BukkitScheduler} tasks a {@link Player} has.
     */
    public static List<Integer> getTasks(Player player) { return playerTasks.getOrDefault(player, null); }

    /** Removes a {@link Player} from the map.
     * @param player The {@link Player}.
     */
    public static void remove(Player player) { playerTasks.remove(player); }

    /** Adds a {@link org.bukkit.scheduler.BukkitScheduler} task under the name of a {@link Player}.
     * @param player The {@link Player}.
     * @param task The task.
     */
    public static void addTask(Player player, int task) {
        List<Integer> tasks = getTasks(player);
        if (tasks == null) {
            tasks = new ArrayList<>();
            playerTasks.put(player, tasks);
        }
        tasks.add(task);
    }

    /** Adds a {@link org.bukkit.scheduler.BukkitScheduler} task under the name of a {@link Player}, and schedules it.
     * @param player The {@link Player}.
     * @param plugin The {@link Plugin} the task is under.
     * @param run The {@link Runnable}.
     * @param delay The delay until running.
     * @return The scheduled task's ID.
     */
    public static int addTask(Player player, Plugin plugin, Runnable run, long delay) {
        int task = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, run, delay);
        addTask(player, task);
        return task;
    }

    /** Removes a task from a {@link Player}.
     * @param player The {@link Player}.
     * @param task The task.
     */
    public static void removeTask(Player player, int task) {
        List<Integer> tasks = getTasks(player);
        if (tasks != null)
            tasks.remove(task);
    }

    /** Cancels and removes a task from a {@link Player}.
     * @param player The {@link Player}.
     * @param task The task.
     */
    public static void cancelTask(Player player, int task) {
        Bukkit.getScheduler().cancelTask(task);
        removeTask(player, task);
    }

    /** Cancels all tasks a {@link Player} has.
     * @param player The {@link Player}.
     */
    public static void cancelTasks(Player player) {
        List<Integer> tasks = getTasks(player);
        if (tasks != null)
            tasks.forEach(Bukkit.getScheduler()::cancelTask);
    }

    /** Updates all tasks a {@link Player} has, removing ones which are complete.
     * @param player The {@link Player}.
     */
    public static void updateTasks(Player player) {
        List<Integer> tasks = getTasks(player);
        if (tasks != null)
            tasks.removeIf(r -> !Bukkit.getScheduler().isQueued(r));
    }
}

