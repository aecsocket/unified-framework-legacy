package me.aecsocket.unifiedframework.utils.persistence.types;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/** A {@link PersistentDataType} for storing {@link String} arrays. */
public class StringArrayDataType implements PersistentDataType<byte[], String[]> {
    private Charset charset;

    public StringArrayDataType(Charset charset) {
        this.charset = charset;
    }

    public StringArrayDataType() {
        this(StandardCharsets.UTF_8);
    }

    /** Gets the {@link Charset} used in conversions.
     * @return The {@link Charset} used in conversions.
     */
    public Charset getCharset() { return charset; }

    @Override
    public Class<byte[]> getPrimitiveType() { return byte[].class; }

    @Override
    public Class<String[]> getComplexType() { return String[].class; }

    @Override
    public byte[] toPrimitive(String[] strings, PersistentDataAdapterContext context) {
        byte[][] bytes = new byte[strings.length][];
        int size = 0;
        for (int i = 0; i < bytes.length; i++) {
            byte[] stringBytes = strings[i].getBytes();
            bytes[i] = stringBytes;
            size += stringBytes.length;
        }

        ByteBuffer buffer = ByteBuffer.allocate(size + bytes.length * 4);
        for (byte[] stringBytes : bytes) {
            buffer.putInt(stringBytes.length);
            buffer.put(stringBytes);
        }

        return buffer.array();
    }

    @Override
    public String[] fromPrimitive(byte[] bytes, PersistentDataAdapterContext context) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        List<String> strings = new ArrayList<>();

        while (buffer.remaining() > 0) {
            if (buffer.remaining() < 4)
                break;

            int length = buffer.getInt();
            if (buffer.remaining() < length)
                break;

            byte[] stringBytes = new byte[length];
            buffer.get(stringBytes);

            strings.add(new String(stringBytes, charset));
        }

        return strings.toArray(new String[0]);
    }
}
