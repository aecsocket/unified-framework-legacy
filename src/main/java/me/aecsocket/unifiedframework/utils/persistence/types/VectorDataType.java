package me.aecsocket.unifiedframework.utils.persistence.types;

import me.aecsocket.unifiedframework.utils.persistence.PersistenceUtils;
import me.aecsocket.unifiedframework.utils.persistence.PluginDataType;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

/** A {@link PersistentDataType} for storing {@link org.bukkit.util.Vector}s. */
public class VectorDataType extends PluginDataType<PersistentDataContainer, Vector> {
    public VectorDataType(Plugin plugin) {
        super(plugin);
    }

    @Override
    public Class<PersistentDataContainer> getPrimitiveType() { return PersistentDataContainer.class; }

    @Override
    public Class<Vector> getComplexType() { return Vector.class; }

    @Override
    public PersistentDataContainer toPrimitive(Vector vector, PersistentDataAdapterContext context) {
        PersistentDataContainer data = context.newPersistentDataContainer();
        data.set(getKey("x"), PersistentDataType.DOUBLE, vector.getX());
        data.set(getKey("y"), PersistentDataType.DOUBLE, vector.getY());
        data.set(getKey("z"), PersistentDataType.DOUBLE, vector.getY());
        return data;
    }

    @Override
    public Vector fromPrimitive(PersistentDataContainer data, PersistentDataAdapterContext context) {
        return new Vector(
                PersistenceUtils.get(data, getKey("x"), PersistentDataType.DOUBLE),
                PersistenceUtils.get(data, getKey("y"), PersistentDataType.DOUBLE),
                PersistenceUtils.get(data, getKey("z"), PersistentDataType.DOUBLE));
    }
}
