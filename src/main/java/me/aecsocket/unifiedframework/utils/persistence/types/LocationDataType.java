package me.aecsocket.unifiedframework.utils.persistence.types;

import me.aecsocket.unifiedframework.utils.persistence.PersistenceUtils;
import me.aecsocket.unifiedframework.utils.persistence.PluginDataType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

/** A {@link PersistentDataType} for storing {@link Location}s. */
public class LocationDataType extends PluginDataType<PersistentDataContainer, Location> {
    public LocationDataType(Plugin plugin) {
        super(plugin);
    }

    @Override
    public Class<PersistentDataContainer> getPrimitiveType() { return PersistentDataContainer.class; }

    @Override
    public Class<Location> getComplexType() { return Location.class; }

    @Override
    public PersistentDataContainer toPrimitive(Location location, PersistentDataAdapterContext context) {
        PersistentDataContainer data = context.newPersistentDataContainer();
        if (location.getWorld() != null)
            data.set(getKey("world"), PersistentDataType.STRING, location.getWorld().getName());
        data.set(getKey("x"), PersistentDataType.DOUBLE, location.getX());
        data.set(getKey("y"), PersistentDataType.DOUBLE, location.getY());
        data.set(getKey("z"), PersistentDataType.DOUBLE, location.getY());
        if (location.getPitch() != 0)
            data.set(getKey("pitch"), PersistentDataType.FLOAT, location.getPitch());
        if (location.getYaw() != 0)
            data.set(getKey("yaw"), PersistentDataType.FLOAT, location.getYaw());
        return data;
    }

    @Override
    public Location fromPrimitive(PersistentDataContainer data, PersistentDataAdapterContext context) {
        String worldName = PersistenceUtils.getOrDefault(data, getKey("world"), PersistentDataType.STRING, null);
        World world = (worldName == null) ? null : Bukkit.getWorld(worldName);
        return new Location(world,
                PersistenceUtils.get(data, getKey("x"), PersistentDataType.DOUBLE),
                PersistenceUtils.get(data, getKey("y"), PersistentDataType.DOUBLE),
                PersistenceUtils.get(data, getKey("z"), PersistentDataType.DOUBLE),
                PersistenceUtils.getOrDefault(data, getKey("pitch"), PersistentDataType.FLOAT, 0f),
                PersistenceUtils.getOrDefault(data, getKey("yaw"), PersistentDataType.FLOAT, 0f));
    }
}
