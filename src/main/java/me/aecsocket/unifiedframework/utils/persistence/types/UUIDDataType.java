package me.aecsocket.unifiedframework.utils.persistence.types;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;

import java.nio.ByteBuffer;
import java.util.UUID;

/** A {@link PersistentDataType} for storing {@link Vector}s. */
public class UUIDDataType implements PersistentDataType<byte[], UUID> {
    @Override
    public Class<byte[]> getPrimitiveType() { return byte[].class; }
    @Override
    public Class<UUID> getComplexType() { return UUID.class; }

    @Override
    public byte[] toPrimitive(UUID complex, PersistentDataAdapterContext context) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(complex.getMostSignificantBits());
        bb.putLong(complex.getLeastSignificantBits());
        return bb.array();
    }

    @Override
    public UUID fromPrimitive(byte[] primitive, PersistentDataAdapterContext context) {
        ByteBuffer bb = ByteBuffer.wrap(primitive);
        long most = bb.getLong();
        long least = bb.getLong();
        return new UUID(most, least);
    }
}
