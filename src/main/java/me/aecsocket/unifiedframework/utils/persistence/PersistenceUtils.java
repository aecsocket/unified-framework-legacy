package me.aecsocket.unifiedframework.utils.persistence;

import org.bukkit.NamespacedKey;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

/** Utility class for {@link PersistentDataType}s. */
public final class PersistenceUtils {
    private PersistenceUtils() {}

    /** Gets data from a {@link PersistentDataContainer}.
     * @param data The {@link PersistentDataContainer}.
     * @param key The {@link NamespacedKey} of the data.
     * @param type The {@link PersistentDataType} of the data.
     * @param <T> The primitive type.
     * @param <Z> The complex type.
     * @return The {@link Z}.
     */
    public static <T, Z> Z get(PersistentDataContainer data, NamespacedKey key, PersistentDataType<T, Z> type) {
        if (data.has(key, type))
            return data.get(key, type);
        throw new PersistenceException("Container had no value '" + key + "' of type " + type);
    }
    /** Gets data from a {@link PersistentDataContainer} or a default value if there is none.
     * @param data The {@link PersistentDataContainer}.
     * @param key The {@link NamespacedKey} of the data.
     * @param type The {@link PersistentDataType} of the data.
     * @param defaultObject The default {@link Z}.
     * @param <T> The primitive type.
     * @param <Z> The complex type.
     * @return The {@link Z}.
     */
    public static <T, Z> Z getOrDefault(PersistentDataContainer data, NamespacedKey key, PersistentDataType<T, Z> type, Z defaultObject) {
        if (data.has(key, type))
            return data.get(key, type);
        return defaultObject;
    }
}
