package me.aecsocket.unifiedframework.utils.persistence.map;

import me.aecsocket.unifiedframework.utils.persistence.PersistenceUtils;
import me.aecsocket.unifiedframework.utils.persistence.PluginDataType;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class MapDataType<K, V> extends PluginDataType<PersistentDataContainer, Map> {
    private MapEntryDataType<K, V> dataType;

    public MapDataType(Plugin plugin, MapEntryDataType<K, V> dataType) {
        super(plugin);
        this.dataType = dataType;
    }

    public MapEntryDataType<K, V> getDataType() { return dataType; }

    @Override
    public @NotNull Class<PersistentDataContainer> getPrimitiveType() { return PersistentDataContainer.class; }

    @Override
    public @NotNull Class<Map> getComplexType() { return Map.class; }

    @Override
    public @NotNull PersistentDataContainer toPrimitive(Map map, PersistentDataAdapterContext context) {
        PersistentDataContainer data = context.newPersistentDataContainer();
        int size = map.size();

        data.set(getKey("size"), PersistentDataType.INTEGER, size);
        for (int i = 0; i < size; i++) {
            data.set(getKey(Integer.toString(i)), PersistentDataType.TAG_CONTAINER, dataType.toPrimitive((Map.Entry<?, ?>)map.get(i), context));
        }

        return data;
    }

    @Override
    public @NotNull Map<K, V> fromPrimitive(@NotNull PersistentDataContainer data, @NotNull PersistentDataAdapterContext context) {
        int size = PersistenceUtils.get(data, getKey("size"), PersistentDataType.INTEGER);
        HashMap<K, V> map = new HashMap<>();
        for (int i = 0; i < size; i++) {
            Map.Entry<K, V> entry = PersistenceUtils.get(data, getKey("i" + i), dataType);
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }
}
