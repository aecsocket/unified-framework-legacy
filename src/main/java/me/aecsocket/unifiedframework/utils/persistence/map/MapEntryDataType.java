package me.aecsocket.unifiedframework.utils.persistence.map;

import me.aecsocket.unifiedframework.utils.persistence.PersistenceUtils;
import me.aecsocket.unifiedframework.utils.persistence.PluginDataType;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.AbstractMap;
import java.util.Map;

public class MapEntryDataType<K, V> extends PluginDataType<PersistentDataContainer, Map.Entry> {
    private Class<K> keyType;
    private Class<V> valueType;

    private PersistentDataType<?, K> keyDataType;
    private PersistentDataType<?, V> valueDataType;

    public MapEntryDataType(Plugin plugin, Class<K> keyType, Class<V> valueType, PersistentDataType<?, K> keyDataType, PersistentDataType<?, V> valueDataType) {
        super(plugin);
        this.keyType = keyType;
        this.valueType = valueType;

        this.keyDataType = keyDataType;
        this.valueDataType = valueDataType;
    }

    public Class<K> getKeyType() { return keyType; }
    public Class<V> getValueType() { return valueType; }

    public PersistentDataType<?, K> getKeyDataType() { return keyDataType; }
    public void setKeyDataType(PersistentDataType<?, K> keyDataType) { this.keyDataType = keyDataType; }

    public PersistentDataType<?, V> getValueDataType() { return valueDataType; }
    public void setValueDataType(PersistentDataType<?, V> valueDataType) { this.valueDataType = valueDataType; }

    @Override
    public @NotNull Class<PersistentDataContainer> getPrimitiveType() { return PersistentDataContainer.class; }

    @Override

    public @NotNull Class<Map.Entry> getComplexType() { return Map.Entry.class; }

    @Override
    public @NotNull PersistentDataContainer toPrimitive(Map.Entry entry, PersistentDataAdapterContext context) {
        PersistentDataContainer data = context.newPersistentDataContainer();
        data.set(getKey("k"), keyDataType, keyType.cast(entry.getKey()));
        data.set(getKey("v"), valueDataType, valueType.cast(entry.getValue()));
        return data;
    }

    @Override
    public @NotNull Map.Entry<K, V> fromPrimitive(@NotNull PersistentDataContainer data, @NotNull PersistentDataAdapterContext context) {
        K key = PersistenceUtils.get(data, getKey("k"), keyDataType);
        V value = PersistenceUtils.get(data, getKey("v"), valueDataType);
        return new AbstractMap.SimpleEntry<>(key, value);
    }
}
