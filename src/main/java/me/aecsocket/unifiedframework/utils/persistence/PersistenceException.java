package me.aecsocket.unifiedframework.utils.persistence;

public class PersistenceException extends RuntimeException {
    public PersistenceException(String message) {
        super(message);
    }
}
