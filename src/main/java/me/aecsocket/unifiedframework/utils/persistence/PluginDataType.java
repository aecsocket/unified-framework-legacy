package me.aecsocket.unifiedframework.utils.persistence;

import org.bukkit.NamespacedKey;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

/** A wrapper around {@link PersistentDataType} which allows storing and getting {@link NamespacedKey}s easily.
 * @param <T> The primitive type.
 * @param <Z> The complex type.
 */
public abstract class PluginDataType<T, Z> implements PersistentDataType<T, Z> {
    private Plugin plugin;
    private Map<String, NamespacedKey> keys;

    public PluginDataType(Plugin plugin) {
        this.plugin = plugin;
        this.keys = new HashMap<>();
    }

    /** Gets the {@link Plugin} used to make {@link NamespacedKey}s.
     * @return The {@link Plugin} used to make {@link NamespacedKey}s.
     */
    public Plugin getPlugin() { return plugin; }

    /** Gets a {@link Map} of named {@link NamespacedKey}s.
     * @return A {@link Map} of named {@link NamespacedKey}s.
     */
    public Map<String, NamespacedKey> getKeys() { return keys; }

    /** Gets a specific {@link NamespacedKey}, or creates one if it does not exist already.
     * @param name The key name.
     * @return The {@link NamespacedKey}.
     */
    public NamespacedKey getKey(String name) {
        if (!keys.containsKey(name))
            keys.put(name, new NamespacedKey(plugin, name));
        return keys.get(name);
    }
}
