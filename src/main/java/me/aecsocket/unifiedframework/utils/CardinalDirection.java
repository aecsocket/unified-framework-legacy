package me.aecsocket.unifiedframework.utils;

import org.bukkit.Location;

public class CardinalDirection {
    public interface Order {}

    public enum Order1 implements Order {
        NORTH,
        EAST,
        SOUTH,
        WEST;

        public static Order1 get(float yaw) { return values()[getOrdinal(yaw, 1)]; }
    }

    public enum Order2 implements Order {
        NORTH,
        NORTH_EAST,
        EAST,
        SOUTH_EAST,
        SOUTH,
        SOUTH_WEST,
        WEST,
        NORTH_WEST;

        public static Order2 get(float yaw) { return values()[getOrdinal(yaw, 2)]; }
    }

    public static int getOrdinal(float yaw, int order) {
        order *= 4;
        float segment = 360f / order;
        yaw = ((Location.normalizeYaw(yaw) + 180) + (segment / 2)) % 360;
        return (int)((yaw / 360) * order);
    }
}