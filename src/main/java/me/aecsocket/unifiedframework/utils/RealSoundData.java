package me.aecsocket.unifiedframework.utils;

import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/** Plays a sound with a fine control over how loud it sounds depending on the distance from it.
 * This accurately models the speed of sound, taking longer when the source is further away using real but intensive calculations.  */
public class RealSoundData extends DistanceSoundData implements Cloneable {
    public static final double DEFAULT_SPEED = 340.29;

    private transient Plugin plugin;
    private double speed = DEFAULT_SPEED;

    public RealSoundData(String sound, SoundCategory category, float volume, float pitch, double dropoffStart, double maxDistance, Plugin plugin, double speed) {
        super(sound, category, volume, pitch, dropoffStart, maxDistance);
        this.plugin = plugin;
        this.speed = speed;
    }

    public RealSoundData(String sound, SoundCategory category, float volume, float pitch, double dropoffStart, double maxDistance, Plugin plugin) {
        this(sound, category, volume, pitch, dropoffStart, maxDistance, plugin, DEFAULT_SPEED);
    }

    public RealSoundData() {}

    public Plugin getPlugin() { return plugin; }
    public void setPlugin(Plugin plugin) { this.plugin = plugin; }

    public double getSpeed() { return speed; }
    public void setSpeed(double speed) { this.speed = speed; }

    @Override
    public void play(@NotNull Player player, @NotNull Location location) {
        double distance = player.getLocation().distance(location);
        if (distance > getMaxDistance())
            return;
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> super.play(player, location), getTravelTime(distance, speed));
    }

    public static int getTravelTime(double distance, double speed) { return (int)(distance / (speed / SchedulerLoop.TICKS_PER_SECOND)); }

    public RealSoundData clone() { return (RealSoundData)super.clone(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RealSoundData that = (RealSoundData)o;
        return Double.compare(that.speed, speed) == 0 &&
                Objects.equals(plugin, that.plugin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), plugin, speed);
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
