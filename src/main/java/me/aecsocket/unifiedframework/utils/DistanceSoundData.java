package me.aecsocket.unifiedframework.utils;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Location;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/** Plays a sound with a fine control over how loud it sounds depending on the distance from it. */
public class DistanceSoundData extends SoundData implements Cloneable {
    private double dropoffStart;
    private double maxDistance;

    public DistanceSoundData(String sound, SoundCategory category, float volume, float pitch, double dropoffStart, double maxDistance) {
        super(sound, category, volume, pitch);
        this.dropoffStart = dropoffStart;
        this.maxDistance = maxDistance;
    }

    public DistanceSoundData() {}

    /** Gets the distance at which volume dropoff starts.
     * @return The distance at which volume dropoff starts.
     */
    public double getDropoffStart() { return dropoffStart; }

    /** Sets the distance at which volume dropoff starts.
     * @param dropoffStart The distance at which volume dropoff starts.
     */
    public void setDropoffStart(double dropoffStart) { this.dropoffStart = dropoffStart; }

    /** Gets the maximum distance that the sound can be heard from.
     * @return The maximum distance that the sound can be heard from.
     */
    public double getMaxDistance() { return maxDistance; }

    /** Sets the maximum distance that the sound can be heard from.
     * @param maxDistance The maximum distance that the sound can be heard from.
     */
    public void setMaxDistance(double maxDistance) { this.maxDistance = maxDistance; }

    @Override
    public void play(@NotNull Player player, @NotNull Location location) {
        if (maxDistance <= 0) {
            player.playSound(location, getSound(), getCategory(), getVolume(), getPitch());
            return;
        }

        Location playerLoc = player.getLocation();
        double distance = location.distance(playerLoc);
        float volume = (float)(1 - Math.min(1, Math.max(0, (distance - dropoffStart) / (maxDistance - dropoffStart)))) * getVolume();

        if (volume > 0)
            player.playSound(playerLoc, getSound(), getCategory(), volume, getPitch());
    }

    public DistanceSoundData clone() { return (DistanceSoundData)super.clone(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DistanceSoundData that = (DistanceSoundData)o;
        return Double.compare(that.dropoffStart, dropoffStart) == 0 &&
                Double.compare(that.maxDistance, maxDistance) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dropoffStart, maxDistance);
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
