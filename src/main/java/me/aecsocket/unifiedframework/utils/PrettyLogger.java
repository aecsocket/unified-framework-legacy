package me.aecsocket.unifiedframework.utils;

import org.bukkit.plugin.Plugin;

/** A pretty output logger. */
public class PrettyLogger {
    /** A level of logging. */
    public enum Level {
        VERBOSE     (-1, "VERB", 4, 7, 7),
        INFO        (0, "INFO", 2, 0, 7),
        WARNING     (1, "WARN", 3, 0, 3),
        ERROR       (2, "ERR ", 1, 7, 9);

        private int level;
        private String prefix;
        private int background;
        private int prefixForeground;
        private int foreground;

        Level(int level, String prefix, int background, int prefixForeground, int foreground) {
            this.level = level;
            this.prefix = prefix;
            this.background = background;
            this.prefixForeground = prefixForeground;
            this.foreground = foreground;
        }

        public int getLevel() { return level; }
        public String getPrefix() { return prefix; }
        public int getBackground() { return background; }
        public int getPrefixForeground() { return prefixForeground; }
        public int getForeground() { return foreground; }

        public String generatePrefix() {
            return "\033[48;5;" +
                    background +
                    ";38;5;" +
                    prefixForeground +
                    "m " +
                    prefix +
                    " " +
                    "\033[49;38;5;" +
                    foreground +
                    "m";
        }
    }

    /** The default logging {@link Level}. */
    public static final Level DEFAULT = Level.INFO;

    private final Plugin plugin;
    private Level level;

    public PrettyLogger(Plugin plugin, Level level) {
        this.plugin = plugin;
        this.level = level;
    }

    public PrettyLogger(Plugin plugin) {
        this(plugin, DEFAULT);
    }

    public Plugin getPlugin() { return plugin; }

    public Level getLevel() { return level; }
    public void setLevel(Level level) { this.level = level; }

    /** Logs objects using a {@link Level}.
     * @param level The {@link Level}.
     * @param message The message(s).
     */
    public void log(Level level, Object... message) {
        if (level.level >= this.level.level) {
            StringBuilder str = new StringBuilder();
            str.append(level.generatePrefix());
            str.append(" ");
            for (Object obj : message)
                str.append(obj);
            str.append("\033[0m");
            plugin.getLogger().log(java.util.logging.Level.INFO, str.toString());
        }
    }
}
