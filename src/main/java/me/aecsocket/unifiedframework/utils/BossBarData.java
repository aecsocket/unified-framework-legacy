package me.aecsocket.unifiedframework.utils;

import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;

/* Stores information about displaying a {@link BossBar} to a {@link Player}. */
public class BossBarData implements Cloneable {
    private String title;
    private BarColor color;
    private BarStyle style;
    private double progress;

    public BossBarData(String title, BarColor color, BarStyle style, double progress) {
        this.title = title;
        this.color = color;
        this.style = style;
        setProgress(progress);
    }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public BarColor getColor() { return color; }
    public void setColor(BarColor color) { this.color = color; }

    public BarStyle getStyle() { return style; }
    public void setStyle(BarStyle style) { this.style = style; }

    public double getProgress() { return progress; }
    public void setProgress(double progress) { this.progress = Math.min(1, Math.max(0, progress)); }

    /** Applies all attributes to a {@link BossBar}.
     * @param bar The {@link BossBar}.
     */
    public void apply(BossBar bar) {
        bar.setTitle(title);
        bar.setColor(color);
        bar.setStyle(style);
        bar.setProgress(progress);
    }

    public BossBarData clone() { try { return (BossBarData)super.clone(); } catch (CloneNotSupportedException e) { return null; } }
}
