package me.aecsocket.unifiedframework.utils;

import org.bukkit.ChatColor;

import java.util.Arrays;

/** A collection of {@link ChatColor}s to be used as part of a string. */
public class ChatColors {
    private ChatColor[] colors;

    public ChatColors(ChatColor... colors) {
        this.colors = colors;
    }

    /** Gets the array of {@link ChatColor}s.
     * @return The array of {@link ChatColor}s.
     */
    public ChatColor[] getColors() { return colors; }

    /** Sets the array of {@link ChatColor}s.
     * @param colors The array of {@link ChatColor}s.
     */
    public void setColors(ChatColor... colors) { this.colors = colors; }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (ChatColor color : colors)
            str.append(color.toString());
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatColors that = (ChatColors)o;
        return Arrays.equals(colors, that.colors);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(colors);
    }
}
