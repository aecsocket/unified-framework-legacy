package me.aecsocket.unifiedframework.utils.projectile;

import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

/** An object which has physics and can collide. */
public interface IProjectile extends Tickable {
    /** Projectiles beneath this speed will be considered stopped. */
    double SPEED_THRESHOLD = 0.05;
    /** The default amount projectiles move down per tick. */
    double GRAVITY = 9.8;
    /** The default amount of steps a projectile takes. */
    int STEPS = 4;
    /** The default {@link FluidCollisionMode}. */
    FluidCollisionMode FLUID_COLLISION_MODE = FluidCollisionMode.NEVER;
    /** The default setting for projectiles moving through passable blocks. */
    boolean IGNORE_PASSABLE_BLOCKS = true;
    /** The default bounce amount. */
    double BOUNCE = 0;

    Location getLocation();
    void setLocation(Location location);

    Vector getVelocity();
    void setVelocity(Vector velocity);

    double getBounce();
    void setBounce(double bounce);

    double getDrag();
    void setDrag(double drag);

    double getGravity();
    void setGravity(double gravity);

    int getSteps();
    void setSteps(int steps);

    int getStepsTraveled();
    void setStepsTraveled(int stepsTraveled);

    double getDistanceTraveled();
    void setDistanceTraveled(double distanceTraveled);

    @Override
    default void tick(TickContext context) {
        for (int i = 0; i < getSteps(); i++) {
            step(context);
            if (context.isRemoved())
                return;
        }
    }

    /** Steps once through the projectile's physics.
     * @param context The {@link TickContext} of this tick.
     */
    void step(TickContext context);

    /** When the projectile hits a block or an entity.
     * @param ray The {@link RayTraceResult}.
     * @param context The {@link TickContext} of the step.
     * @return The {@link ProjectileHitResult}.
     */
    ProjectileHitResult hit(RayTraceResult ray, TickContext context);
}
