package me.aecsocket.unifiedframework.utils.projectile;

import me.aecsocket.unifiedframework.loop.Loop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.function.Predicate;

/** An object which has physics and can collide. */
public class Projectile implements IProjectile {
    private Location location;
    private Vector velocity;
    private double bounce;
    private double drag;
    private double gravity;
    private int steps;
    private FluidCollisionMode fluidCollisionMode;
    private boolean ignorePassableBlocks;
    private double size;
    private Entity entity;
    private Predicate<Entity> predicate;

    private World world;
    private double stepLength;
    private int stepsTraveled;
    private double distanceTraveled;
    private Block currentBlock;
    private Entity currentEntity;
    private ProjectileHitType currentType;
    private boolean ignoreOrigin = true;

    public Projectile(Location location, Vector velocity, double bounce, double drag, double gravity, int steps, FluidCollisionMode fluidCollisionMode, boolean ignorePassableBlocks, double size, Entity entity, Predicate<Entity> predicate) {
        this.location = location;
        this.velocity = velocity;
        this.bounce = bounce;
        this.drag = drag;
        this.gravity = gravity;
        this.steps = steps;
        this.fluidCollisionMode = fluidCollisionMode;
        this.ignorePassableBlocks = ignorePassableBlocks;
        this.size = size;
        this.entity = entity;
        this.predicate = predicate;

        this.world = location.getWorld();
        this.stepLength = 0.0d;
        this.stepsTraveled = 0;
        this.distanceTraveled = 0.0d;
        this.currentBlock = null;
        this.currentEntity = null;
        this.currentType = null;
    }

    public Projectile(Location location, Vector velocity, double bounce, double drag, int steps, FluidCollisionMode fluidCollisionMode, boolean ignorePassableBlocks, double size, Entity entity, Predicate<Entity> predicate) {
        this(location, velocity, bounce, drag, GRAVITY, steps, fluidCollisionMode, ignorePassableBlocks, size, entity, predicate);
    }

    public Projectile(Location location, Vector velocity, double bounce, double drag, int steps, FluidCollisionMode fluidCollisionMode, boolean ignorePassableBlocks, Entity entity, Predicate<Entity> predicate) {
        this(location, velocity, bounce, drag, GRAVITY, steps, fluidCollisionMode, ignorePassableBlocks, 0, entity, predicate);
    }

    public Projectile(Location location, Vector velocity, double bounce, double drag, FluidCollisionMode fluidCollisionMode, boolean ignorePassableBlocks, Entity entity, Predicate<Entity> predicate) {
        this(location, velocity, bounce, drag, GRAVITY, STEPS, fluidCollisionMode, ignorePassableBlocks, 0, entity, predicate);
    }

    public Projectile(Location location, Vector velocity, double bounce, double drag, Entity entity, Predicate<Entity> predicate) {
        this(location, velocity, bounce, drag, FLUID_COLLISION_MODE, IGNORE_PASSABLE_BLOCKS, entity, predicate);
    }

    public Projectile(Location location, Vector velocity, double drag, Entity entity) {
        this(location, velocity, BOUNCE, drag, entity, null);
    }

    @Override
    public Location getLocation() { return location; }
    @Override
    public void setLocation(Location location) { this.location = location; }

    @Override
    public Vector getVelocity() { return velocity; }
    @Override
    public void setVelocity(Vector velocity) { this.velocity = velocity; }

    @Override
    public double getBounce() { return bounce; }
    @Override
    public void setBounce(double bounce) { this.bounce = bounce; }

    @Override
    public double getDrag() { return drag; }
    @Override
    public void setDrag(double drag) { this.drag = drag; }

    @Override
    public double getGravity() { return gravity; }
    @Override
    public void setGravity(double gravity) { this.gravity = gravity; }

    @Override
    public int getSteps() { return steps; }
    @Override
    public void setSteps(int steps) { this.steps = steps; }

    public FluidCollisionMode getFluidCollisionMode() { return fluidCollisionMode; }
    public void setFluidCollisionMode(FluidCollisionMode fluidCollisionMode) { this.fluidCollisionMode = fluidCollisionMode; }

    public boolean ignoresPassableBlocks() { return ignorePassableBlocks; }
    public void setIgnorePassableBlocks(boolean ignorePassableBlocks) { this.ignorePassableBlocks = ignorePassableBlocks; }

    public double getSize() { return size; }
    public void setSize(double size) { this.size = size; }

    public Entity getEntity() { return entity; }
    public void setEntity(Entity entity) { this.entity = entity; }

    public Predicate<Entity> getPredicate() { return predicate; }
    public void setPredicate(Predicate<Entity> predicate) { this.predicate = predicate; }

    public World getWorld() { return world; }
    /** Gets how far the last step was.
     * @return How far the last step was.
     */
    public double getStepLength() { return stepLength; }

    @Override
    public int getStepsTraveled() { return stepsTraveled; }
    @Override
    public void setStepsTraveled(int stepsTraveled) { this.stepsTraveled = stepsTraveled; }

    @Override
    public double getDistanceTraveled() { return distanceTraveled; }
    @Override
    public void setDistanceTraveled(double distanceTraveled) { this.distanceTraveled = distanceTraveled; }

    /** Gets the current {@link Block} the projectile is in, if {@link Projectile#hit(RayTraceResult, TickContext)} returned {@link ProjectileHitResult#CONTINUE}.
     * @return The current {@link Block} the projectile is in, if {@link Projectile#hit(RayTraceResult, TickContext)} returned {@link ProjectileHitResult#CONTINUE}.
     */
    public Block getCurrentBlock() { return currentBlock; }

    /** Gets the current {@link Entity} the projectile is in, if {@link Projectile#hit(RayTraceResult, TickContext)} returned {@link ProjectileHitResult#CONTINUE}.
     * @return The current {@link Entity} the projectile is in, if {@link Projectile#hit(RayTraceResult, TickContext)} returned {@link ProjectileHitResult#CONTINUE}.
     */
    public Entity getCurrentEntity() { return currentEntity; }

    /** Gets the current {@link ProjectileHitType} of the object the projectile is in, if {@link Projectile#hit(RayTraceResult, TickContext)} returned {@link ProjectileHitResult#CONTINUE}.
     * @return The current {@link ProjectileHitType} of the object the projectile is in, if {@link Projectile#hit(RayTraceResult, TickContext)} returned {@link ProjectileHitResult#CONTINUE}.
     */
    public ProjectileHitType getCurrentType() { return currentType; }

    public boolean ignoresOrigin() { return ignoreOrigin; }
    public void setIgnoreOrigin(boolean ignoreOrigin) { this.ignoreOrigin = ignoreOrigin; }

    /** Steps once through the projectile's physics.
     * @param context The {@link TickContext} of this tick.
     */
    @Override
    public void step(TickContext context) {
        ++stepsTraveled;
        Loop loop = context.getLoop();
        double stepMultiplier = (loop.getPeriod() / 1000d) / steps;
        Vector step = velocity.clone().multiply(stepMultiplier);
        stepLength = step.length();

        if (location.getY() < 0) {
            context.removeTickable();
            return;
        }
        if (step.length() != 0) {
            RayTraceResult ray = world.rayTrace(location, step, stepLength, fluidCollisionMode, ignorePassableBlocks, size, predicate);
            if (ray != null && (!ignoreOrigin || ray.getHitEntity() != entity)) {
                boolean handleHit = true;
                if (currentType != null) {
                    if (currentType == ProjectileHitType.BLOCK && ray.getHitBlock() != null && ray.getHitBlock().equals(currentBlock))
                        handleHit = false;
                    if (currentType == ProjectileHitType.ENTITY && ray.getHitEntity() != null && ray.getHitEntity().equals(currentEntity))
                        handleHit = false;
                }


                if (handleHit) {
                    if (currentType == ProjectileHitType.BLOCK && ray.getHitBlock() != currentBlock)
                        currentBlock = ray.getHitBlock();
                    if (currentType == ProjectileHitType.ENTITY && ray.getHitEntity() != currentEntity)
                        currentEntity = ray.getHitEntity();

                    ProjectileHitResult hit = hit(ray, context);
                    if (hit == ProjectileHitResult.BOUNCE) {
                        velocity = FrameworkUtils.reflect(velocity, ray.getHitBlockFace()).multiply(bounce);
                        return;
                    }
                    if (hit == ProjectileHitResult.CONTINUE) {
                        if (ray.getHitBlock() != null) {
                            currentBlock = ray.getHitBlock();
                            currentType = ProjectileHitType.BLOCK;
                        } else if (ray.getHitEntity() != null) {
                            currentEntity = ray.getHitEntity();
                            currentType = ProjectileHitType.ENTITY;
                        }
                    }
                    if (hit == ProjectileHitResult.DESTROY) {
                        context.removeTickable();
                        return;
                    }
                }
            } else if (ignoreOrigin && (ray == null || ray.getHitEntity() != entity))
                ignoreOrigin = false;

            distanceTraveled += stepLength;
            location.add(step);
        }
        double dragMultiplier = drag * stepMultiplier;
        velocity.setX(velocity.getX() - (velocity.getX() * dragMultiplier));
        velocity.setZ(velocity.getZ() - (velocity.getZ() * dragMultiplier));
        velocity.setY(velocity.getY() - (gravity * stepMultiplier));
        velocity.setY(velocity.getY() - (velocity.getY() * dragMultiplier));
    }

    /** When the projectile hits a block or an entity.
     * @param ray The {@link RayTraceResult}.
     * @param context The {@link TickContext} of the step.
     * @return The {@link ProjectileHitResult}.
     */
    @Override
    public ProjectileHitResult hit(RayTraceResult ray, TickContext context) {
        return bounce > 0
            ? (
                    getStepLength() >= SPEED_THRESHOLD
                    ? ProjectileHitResult.BOUNCE
                    : ProjectileHitResult.DESTROY)
            : ProjectileHitResult.DESTROY;
    }
}
