package me.aecsocket.unifiedframework.utils.projectile;

public enum ProjectileHitResult {
    CONTINUE,
    BOUNCE,
    DESTROY
}
