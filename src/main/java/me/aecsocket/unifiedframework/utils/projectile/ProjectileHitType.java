package me.aecsocket.unifiedframework.utils.projectile;

public enum ProjectileHitType {
    BLOCK,
    ENTITY
}
