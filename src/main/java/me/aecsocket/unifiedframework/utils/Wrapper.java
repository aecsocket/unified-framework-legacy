package me.aecsocket.unifiedframework.utils;

/** A wrapper helper class.
 * @param <T> The type this wraps around.
 */
public class Wrapper<T> {
    private final T handle;

    public Wrapper(T handle) {
        this.handle = handle;
    }

    /** Gets the base {@link T}.
     * @return The base {@link T}.
     */
    public T getHandle() { return handle; }
}
