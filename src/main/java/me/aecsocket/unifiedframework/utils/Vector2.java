package me.aecsocket.unifiedframework.utils;

public class Vector2 implements Cloneable {
    private double x;
    private double y;

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2() {}

    public double getX() { return x; }
    public Vector2 setX(double x) { this.x = x; return this; }

    public double getY() { return y; }
    public Vector2 setY(double y) { this.y = y; return this; }

    public Vector2 add(Vector2 other) {
        x += other.x;
        y += other.y;
        return this;
    }
    public Vector2 subtract(Vector2 other) {
        x -= other.x;
        y -= other.y;
        return this;
    }
    public Vector2 multiply(Vector2 other) {
        x *= other.x;
        y *= other.y;
        return this;
    }
    public Vector2 divide(Vector2 other) {
        x /= other.x;
        y /= other.y;
        return this;
    }

    public Vector2 multiply(double amount) {
        x *= amount;
        y *= amount;
        return this;
    }
    public Vector2 zero() {
        x = 0;
        y = 0;
        return this;
    }
    public Vector2 normalize() {
        double length = length();

        x /= length;
        y /= length;

        return this;
    }

    public double manhattanLength() { return Math.abs(x) + Math.abs(y); }
    public double length() { return Math.sqrt(Math.pow(Math.abs(x), 2) + Math.pow(Math.abs(y), 2)); }

    public double manhattanDistance(Vector2 other) { return Math.abs(other.x - x) + Math.abs(other.y - y); }
    public double distance(Vector2 other) { return Math.sqrt(Math.pow(Math.abs(other.x - x), 2) + Math.pow(Math.abs(other.y - y), 2)); }

    public Vector2 clone() { try { return (Vector2)super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    @Override
    public String toString() { return "(" + x + ", " + y + ")"; }
}
