package me.aecsocket.unifiedframework.utils;

import java.util.HashMap;
import java.util.Map;

public class InitMap<K, V> extends HashMap<K, V> {
    public InitMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public InitMap(int initialCapacity) {
        super(initialCapacity);
    }

    public InitMap() {}

    public InitMap(Map<? extends K, ? extends V> m) {
        super(m);
    }

    public InitMap<K, V> add(K key, V value) { put(key, value); return this; }
}
