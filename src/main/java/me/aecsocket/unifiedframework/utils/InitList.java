package me.aecsocket.unifiedframework.utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

public class InitList<T> extends ArrayList<T> {
    public InitList(int initialCapacity) { super(initialCapacity); }
    public InitList() {}
    public InitList(@NotNull Collection<? extends T> c) { super(c); }

    public InitList<T> init(T value) { add(value); return this; }
}
