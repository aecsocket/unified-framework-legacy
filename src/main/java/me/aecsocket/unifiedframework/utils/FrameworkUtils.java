package me.aecsocket.unifiedframework.utils;

import com.google.common.io.CharStreams;
import me.aecsocket.unifiedframework.UnifiedFramework;
import me.aecsocket.unifiedframework.exception.ConfigurationException;
import org.bukkit.Bukkit;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

/** Generic utilities for plugins. */
public final class FrameworkUtils {
    private FrameworkUtils() {}

    /** The default {@link Charset} to use for {@link FrameworkUtils#streamToString(InputStream)}. */
    public static final Charset STREAM_CHARSET = StandardCharsets.UTF_8;
    /** The delimeter for {@link FrameworkUtils#arrayToString(Object[])}. */
    public static final String JOIN_DELIMETER = ", ";
    /** The default {@link DecimalFormat} used for {@link FrameworkUtils#getToTime(long)}. */
    public static final DecimalFormat TIME_FORMAT = new DecimalFormat("0");

    /** A {@link Map} for keeping track of the last drop time for players (to mitigate PlayerInteract also being called on PlayerDropEvent). */
    public static final Map<Player, Integer> LAST_DROP_TIME = new HashMap<>();
    /** A {@link Map} for keeping track of the last click outside inventory time for players (to mitigate PlayerDropEvent also being called on InventoryClickEvent). */
    public static final Map<Player, Integer> LAST_CLICK_TIME = new HashMap<>();

    public static <T extends Enum<T>> boolean isValidEnum(Class<T> clazz, String name) {
        try {
            Enum.valueOf(clazz, name);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /** Gets the full {@link String} from an {@link InputStream}.
     * @param stream The {@link InputStream}.
     * @param charset The {@link Charset} to use.
     * @return The full {@link String}.
     */
    public static String streamToString(InputStream stream, Charset charset) {
        try (Reader reader = new InputStreamReader(stream)) {
            return CharStreams.toString(reader);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /** Gets the full {@link String} from an {@link InputStream}.
     * @param stream The {@link InputStream}.
     * @return The full {@link String}.
     */
    public static String streamToString(InputStream stream) { return streamToString(stream, STREAM_CHARSET); }

    /** Converts an array of {@link String}s to a single {@link String}.
     * @param array The array of {@link String}s.
     * @return The single {@link String}.
     */
    public static String arrayToString(Object[] array) {
        StringBuilder string = new StringBuilder();
        StringJoiner join = new StringJoiner(JOIN_DELIMETER);
        string.append("[");
        for (Object obj : array)
            join.add(obj.toString());
        string.append(join.toString());
        string.append("]");

        return string.toString();
    }

    /** Gets a normalized {@link Vector} of the opposite direction of a {@link BlockFace}.
     * @param face The {@link BlockFace}.
     * @return The {@link Vector}.
     */
    public static Vector getReflectionDirection(BlockFace face) {
        switch (face) {
            case UP:
                return new Vector(0, 1, 0);
            case DOWN:
                return new Vector(0, -1, 0);

            case NORTH:
                return new Vector(0, 0, -1);
            case SOUTH:
                return new Vector(0, 0, 1);

            case EAST:
                return new Vector(-1, 0, 0);
            case WEST:
                return new Vector(1, 0, 0);
            default:
                return null;
        }
    }

    /** Reflects a {@link Vector} off a {@link BlockFace}.
     * @param v The {@link Vector}.
     * @param face The {@link BlockFace}.
     * @return The reflected {@link Vector}.
     */
    public static Vector reflect(Vector v, BlockFace face) {
        Vector v2 = getReflectionDirection(face);
        return v.subtract(v2.multiply(v.dot(v2) * 2.0d));
    }

    /** Gets a string representation of an object by listing values of its fields.
     * @param object The {@link Object} to get the stirng representation of.
     * @param fields The fields of the object to print, or nothing for all declared fields.
     * @return The string representation of the {@link Object}.
     */
    public static String getToString(Object object, String... fields) {
        if (object == null)
            return "null";
        StringBuilder string = new StringBuilder();
        StringJoiner join = new StringJoiner(", ");
        Class<?> clazz = object.getClass();

        if (fields == null || fields.length == 0) {
            List<String> listFields = new ArrayList<>();
            for (Field field : clazz.getDeclaredFields())
                listFields.add(field.getName());
            fields = listFields.toArray(new String[0]);
        }
        for (String fieldName : fields) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                Object value = field.get(object);
                String sValue = value == null ? "null" : value.toString();
                join.add(fieldName + "=" + sValue);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new IllegalArgumentException(e);
            }
        }

        string.append(clazz.getSimpleName());
        string.append("{");
        string.append(join.toString());
        string.append("}");
        return string.toString();
    }

    /** Gets a millisecond value as a string timestamp in the format {1d} {1h} {1m} xs, where {} values are only
     * displayed if they are over 0. "x" signifies the custom format specified in the "ms" parameter.
     * @param millis The millisecond value.
     * @param ms The millisecond {@link DecimalFormat}.
     * @return The string timestamp.
     */
    public static String getToTime(long millis, DecimalFormat ms) {
        long days = TimeUnit.MILLISECONDS.toDays(millis);
        long hours = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;

        StringJoiner join = new StringJoiner(" ");
        if (days > 0)
            join.add(days + "d");
        if (hours > 0)
            join.add(hours + "h");
        if (minutes > 0)
            join.add(minutes + "m");
        join.add(ms.format(((double)millis % (1000 * 60)) / 1000) + "s");

        return join.toString();
    }

    /** Gets a millisecond value as a string timestamp in the format {1d} {1h} {1m} 1s, where {} values are only
     * displayed if they are over 0.
     * @param millis The millisecond value.
     * @return The string timestamp.
     */
    public static String getToTime(long millis) { return getToTime(millis, TIME_FORMAT); }

    /** Gets a config value from a plugin's config.
     @param plugin The {@link Plugin}.
     @param path The path to the key.
     @param <T> The type of the result.
     @return The config value.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getConfigValue(Plugin plugin, String path) {
        FileConfiguration config = plugin.getConfig();
        if (config.contains(path))
            return (T)config.get(path);
        else
            throw new ConfigurationException("No key " + path + " found in " + plugin.getName() + " config");
    }

    /** Gets a player's last {@link org.bukkit.Location} using the metadata value {@link UnifiedFramework#LAST_LOC_META}.
     * @param player The {@link Player}.
     * @return The speed.
     */
    public static Location getLastLocation(Player player) {
        String[] coords = player.getMetadata(UnifiedFramework.LAST_LOC_META).get(0).asString().split(",");
        return new Location(player.getWorld(),
                Double.parseDouble(coords[0]),
                Double.parseDouble(coords[1]),
                Double.parseDouble(coords[2]),
                Float.parseFloat(coords[3]),
                Float.parseFloat(coords[4]));
    }

    /** Gets a player's speed using the metadata value {@link UnifiedFramework#SPEED_META}.
     * @param player The {@link Player}.
     * @return The speed.
     */
    public static double getSpeed(Player player) { return (double)MetadataUtils.getObjectOrDefault(player, UnifiedFramework.SPEED_META, 0.0d); }

    /** Gets a player's vector velocity using the metadata value {@link UnifiedFramework#VELOCITY_META}.
     * @param player The {@link Player}.
     * @return The {@link Vector} velocity.
     */
    public static Vector getVelocity(Player player) {
        String[] coords = player.getMetadata(UnifiedFramework.VELOCITY_META).get(0).asString().split(",");
        return new Vector(
                Double.parseDouble(coords[0]),
                Double.parseDouble(coords[1]),
                Double.parseDouble(coords[2]));
    }

    /** Gets if one location has a direct line of sight to another.
     * @param loc1 The first {@link Location}.
     * @param loc2 The second {@link Location}.
     * @param entityPredicate A {@link Predicate} for the entity hit by the raycast. May be null.
     * @param blockPredicate A {@link Predicate} for the block hit by the raycast. May be null.
     * @param maxPenetration The maximum amount of things this ray can penetrate.
     * @return If one location has a direct line of sight to another.
     */
    public static boolean canSee(Location loc1, Location loc2, @Nullable Predicate<Entity> entityPredicate, @Nullable Predicate<Block> blockPredicate, int maxPenetration) {
        if (loc1.equals(loc2))
            return true;
        double distance = loc1.distance(loc2);
        RayTraceResult result;
        Location loc = loc1.clone();
        Vector dir = loc2.toVector().subtract(loc.toVector()).normalize().multiply(0.1);
        Set<Entity> hitEntities = new HashSet<>();
        Set<Block> hitBlocks = new HashSet<>();
        while ((result = loc.getWorld().rayTrace(loc, dir, distance, FluidCollisionMode.NEVER, true, 0, entityPredicate)) != null) {
            Block block = result.getHitBlock();
            boolean process = true;
            if (maxPenetration > -1) {
                if (block != null) {
                    if (hitBlocks.contains(block))
                        process = false;
                    else
                        hitBlocks.add(block);
                }
                Entity entity = result.getHitEntity();
                if (entity != null) {
                    if (hitEntities.contains(entity))
                        process = false;
                    else
                        hitEntities.add(entity);
                }

                if (hitEntities.size() + hitBlocks.size() >= maxPenetration)
                    return false;
            }
            if (process && (result.getHitBlock() != null && (blockPredicate == null || blockPredicate.test(result.getHitBlock()))))
                return false;
            Location hit = result.getHitPosition().toLocation(loc.getWorld());
            distance -= hit.distance(loc);
            loc = hit;
            loc.add(dir);
        }
        return true;
    }

    /** Gets if one location has a direct line of sight to another.
     * @param loc1 The first {@link Location}.
     * @param loc2 The second {@link Location}.
     * @param entityPredicate A {@link Predicate} for the entity hit by the raycast. May be null.
     * @return If one location has a direct line of sight to another.
     */
    public static boolean canSee(Location loc1, Location loc2, @Nullable Predicate<Entity> entityPredicate) {
        return canSee(loc1, loc2, entityPredicate, null, -1);
    }

    /** Gets if one location has a direct line of sight to another.
     * @param loc1 The first {@link Location}.
     * @param loc2 The second {@link Location}.
     * @return If one location has a direct line of sight to another.
     */
    public static boolean canSee(Location loc1, Location loc2) {
        return canSee(loc1, loc2, null);
    }

    /** Gets an invisible, no-gravity, silent, marker armor stand with all slots disabled.
     * @param location The {@link Location}.
     * @return The marker {@link ArmorStand}.
     */
    public static ArmorStand getMarkerStand(Location location) {
        ArmorStand stand = (ArmorStand)location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        stand.setVisible(false);
        stand.setGravity(false);
        stand.setSilent(true);
        stand.setMarker(true);
        stand.setDisabledSlots(EquipmentSlot.HAND, EquipmentSlot.OFF_HAND, EquipmentSlot.HEAD, EquipmentSlot.CHEST, EquipmentSlot.LEGS, EquipmentSlot.FEET);
        return stand;
    }

    /** Creates a {@link BossBar} with no title, {@link BarColor#WHITE}, {@link BarStyle#SOLID}, and a {@link Player} already added (if provided).
     * @param player The {@link Player} to add.
     * @return The {@link BossBar}.
     */
    public static BossBar createBlankBossBar(@Nullable Player player) {
        BossBar result = Bukkit.createBossBar(null, BarColor.WHITE, BarStyle.SOLID);
        if (player != null)
            result.addPlayer(player);
        result.setProgress(0);
        return result;
    }

    /** Aligns a {@link Location} so an item displayed on the head of an {@link ArmorStand} is aligned with the ground. This modifies the original {@link Location}.
     * @param location The base {@link Location}.
     * @return The altered {@link Location}.
     */
    public static Location alignHeadItem(Location location) { location.subtract(0, 1.37, 0); return location; }

    /** Gets the yaw of a {@link Vector}.
     * @param vector The {@link Vector}.
     * @return The yaw of the {@link Vector}.
     */
    public static float getYaw(Vector vector) {
        double _2PI = 2 * Math.PI;
        double x = vector.getX();
        double z = vector.getZ();

        double theta = Math.atan2(-x, z);
        return Location.normalizeYaw((float)Math.toDegrees((theta + _2PI) % _2PI));
    }

    /** Gets the pitch of a {@link Vector}.
     * @param vector The {@link Vector}.
     * @return The pitch of the {@link Vector}.
     */
    public static float getPitch(Vector vector) {
        double x = vector.getX();
        double z = vector.getZ();

        if (x == 0 && z == 0)
            return vector.getY() > 0 ? -90 : 90;

        double x2 = NumberConversions.square(x);
        double z2 = NumberConversions.square(z);
        double xz = Math.sqrt(x2 + z2);
        return (float)Math.toDegrees(Math.atan(-vector.getY() / xz));
    }

    /** Gets the angle from one {@link Vector} to another based on an existing yaw.
     * @param yaw The yaw.
     * @param base The base {@link Vector}.
     * @param target The target {@link Vector}.
     * @return The angle.
     */
    public static float getYawAngle(float yaw, Vector base, Vector target) {
        Vector delta = target.clone().subtract(base);
        float angle = FrameworkUtils.getYaw(delta);
        return Location.normalizeYaw(angle - yaw);
    }

    /** Determines if a {@link Player} has dropped an item in this tick (to mitigate PlayerInteract also being called on PlayerDropItemEvent).
     * @param player The {@link Player}.
     * @return If they have dropped an item in this tick. If this is run in a PlayerInteractEvent, and this returns true, you should return immediately.
     */
    public static boolean isDropping(Player player) { return LAST_DROP_TIME.containsKey(player) && LAST_DROP_TIME.get(player) == Bukkit.getCurrentTick(); }

    /** Determines if a {@link Player} has clicked an item out of their inventory in this tick (to mitigate PlayerDropItemEvent also being called on InventoryClickEvent).
     * @param player The {@link Player}.
     * @return If they have clicked an item out of their inventory in this tick. If this is run in a PlayerDropItemEvent, and this returns true, you should return immediately.
     */
    public static boolean isClicking(Player player) { return LAST_CLICK_TIME.containsKey(player) && LAST_CLICK_TIME.get(player) == Bukkit.getCurrentTick(); }

    /** Removes a {@link Player} from everything that stores the object.
     * @param player The {@link Player}.
     */
    public static void cleanPlayer(Player player) {
        PlayerTasks.cancelTasks(player);
        PlayerTasks.remove(player);
        LAST_DROP_TIME.remove(player);
        LAST_CLICK_TIME.remove(player);
    }
}
