package me.aecsocket.unifiedframework.utils;

import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** Allows easier access to {@link Metadatable}s. */
public final class MetadataUtils {
    private MetadataUtils() {}

    /** Gets a {@link List} of {@link MetadataValue}s under a key or a default if none are found.
     * @param metadatable The {@link Metadatable}.
     * @param key The key of the {@link MetadataValue}s.
     * @param defaultValue The default value.
     * @return The {@link List} of {@link MetadataValue}s the a default if none are found.
     */
    public static List<MetadataValue> getOrDefault(Metadatable metadatable, String key, MetadataValue defaultValue) { return metadatable.hasMetadata(key) ? metadatable.getMetadata(key) : Collections.singletonList(defaultValue); }

    /** Gets a {@link MetadataValue} under a key or a default if none are found.
     * @param metadatable The {@link Metadatable}.
     * @param key The key of the {@link MetadataValue}s.
     * @param defaultValue The default value.
     * @return The {@link MetadataValue} or a default if none are found.
     */
    public static MetadataValue getValueOrDefault(Metadatable metadatable, String key, MetadataValue defaultValue) { return metadatable.hasMetadata(key) ? metadatable.getMetadata(key).size() > 0 ? metadatable.getMetadata(key).get(0) : defaultValue : defaultValue; }

    /** Gets a {@link MetadataValue}'s value under a key or a default if none are found.
     * @param metadatable The {@link Metadatable}.
     * @param key The key of the {@link MetadataValue}s.
     * @param defaultValue The default value.
     * @return The {@link MetadataValue}'s value under a key or a default if none are found.
     */
    public static Object getObjectOrDefault(Metadatable metadatable, String key, Object defaultValue) { return metadatable.hasMetadata(key) ? metadatable.getMetadata(key).size() > 0 ? metadatable.getMetadata(key).get(0).value() : defaultValue : defaultValue; }
}
