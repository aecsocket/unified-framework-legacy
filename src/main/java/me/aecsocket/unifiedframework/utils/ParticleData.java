package me.aecsocket.unifiedframework.utils;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import java.util.Objects;

/** Stores information about spawning {@link Particle}s in a world or to a player. */
public class ParticleData implements Cloneable {
    private Particle particle;
    private int count;
    private double sizeX;
    private double sizeY;
    private double sizeZ;
    private double speed;
    private Object data;
    private boolean force;

    public ParticleData(Particle particle, int count, double sizeX, double sizeY, double sizeZ, double speed, Object data, boolean force) {
        this.particle = particle;
        this.count = count;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.speed = speed;
        this.data = data;
        this.force = force;
    }

    public ParticleData(Particle particle, int count, double sizeX, double sizeY, double sizeZ, double speed, Object data) {
        this(particle, count, sizeX, sizeY, sizeZ, speed, data, false);
    }

    public ParticleData(Particle particle, int count, double sizeX, double sizeY, double sizeZ, double speed) {
        this(particle, count, sizeX, sizeY, sizeZ, speed, null, false);
    }

    public ParticleData(Particle particle, int count, double size, double speed) {
        this(particle, count, size, size, size, speed, null, false);
    }

    /** Gets the {@link Particle}.
     * @return The {@link Particle}.
     */
    public Particle getParticle() { return particle; }

    /** Sets the {@link Particle}.
     * @param particle The {@link Particle}.
     * @return This new instance.
     */
    public ParticleData setParticle(Particle particle) { this.particle = particle; return this; }

    /** Gets the amount of particles spawned.
     * @return The amount of particles spawned.
     */
    public int getCount() { return count; }

    /** Sets the amount of particles spawned.
     * @param count The amount of particles spawned.
     * @return This new instance.
     */
    public ParticleData setCount(int count) { this.count = count; return this; }

    /** Gets the spawning size in the X axis.
     * @return The spawning size in the X axis.
     */
    public double getSizeX() { return sizeX; }

    /** Sets the spawning size in the X axis.
     * @param sizeX The spawning size in the X axis.
     * @return This new instance.
     */
    public ParticleData setSizeX(double sizeX) { this.sizeX = sizeX; return this; }

    /** Gets the spawning size in the Y axis.
     * @return The spawning size in the Y axis.
     */
    public double getSizeY() { return sizeY; }

    /** Sets the spawning size in the Y axis.
     * @param sizeY The spawning size in the Y axis.
     * @return This new instance.
     */
    public ParticleData setSizeY(double sizeY) { this.sizeY = sizeY; return this; }

    /** Gets the spawning size in the Z axis.
     * @return The spawning size in the Z axis.
     */
    public double getSizeZ() { return sizeZ; }

    /** Sets the spawning size in the Z axis.
     * @param sizeZ The spawning size in the Z axis.
     * @return This new instance.
     */
    public ParticleData setSizeZ(double sizeZ) { this.sizeZ = sizeZ; return this; }

    /** Sets the uniform size of the spawning box.
     * @param size The uniform size of the spawning box.
     * @return This new instance.
     */
    public ParticleData setSize(double size) { this.sizeX = size; this.sizeY = size; this.sizeZ = size; return this; }

    /** Gets the speed of the particles.
     * @return The speed of the particles.
     */
    public double getSpeed() { return speed; }

    /** Sets the speed of the particles.
     * @param speed The speed of the particles.
     * @return This new instance.
     */
    public ParticleData setSpeed(double speed) { this.speed = speed; return this; }

    /** Gets the data used for extra info when drawing particles.
     * @return The data used for extra info when drawing particles.
     */
    public Object getData() { return data; }

    /** Sets the data used for extra info when drawing particles.
     * @param data The data used for extra info when drawing particles.
     * @return This new instance.
     */
    public ParticleData setData(Object data) { this.data = data; return this; }

    /** Gets if the particles are forced.
     * @return If the particles are forced.
     */
    public boolean isForced() { return force; }

    /** Sets if the particles are forced.
     * @param force If the particles are forced.
     * @return This new instance.
     */
    public ParticleData setForced(boolean force) { this.force = force; return this; }

    /** Spawns the particles at a {@link Location}.
     * @param location The {@link Location}.
     */
    public void spawn(Location location) {
        location.getWorld().spawnParticle(particle, location, count, sizeX, sizeY, sizeZ, speed, data, force);
    }

    /** Spawns the particles for a specific {@link Player} at a {@link Location}.
     * @param player The {@link Player}.
     * @param location The {@link Location}.
     */
    public void spawn(Player player, Location location) {
        if (location == null || location.getWorld() == null) return;
        player.spawnParticle(particle, location, count, sizeX, sizeY, sizeZ, speed, data);
    }

    public ParticleData clone() { try { return (ParticleData)super.clone(); } catch (CloneNotSupportedException ignore) { return null; } }

    /** Spawns an array of {@link ParticleData} safely if anything is null.
     * @param data The {@link ParticleData}.
     * @param player The {@link Player} to play to.
     * @param location The {@link Location}.
     */
    public static void spawn(ParticleData[] data, Player player, Location location) {
        if (data != null) {
            for (ParticleData particle : data) {
                if (particle != null)
                    particle.spawn(player, location);
            }
        }
    }

    /** Spawns an array of {@link ParticleData} safely if anything is null.
     * @param data The {@link ParticleData}.
     * @param location The {@link Location}.
     */
    public static void spawn(ParticleData[] data, Location location) {
        if (data != null) {
            for (ParticleData particle : data) {
                if (particle != null)
                    particle.spawn(location);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticleData that = (ParticleData)o;
        return count == that.count &&
                Double.compare(that.sizeX, sizeX) == 0 &&
                Double.compare(that.sizeY, sizeY) == 0 &&
                Double.compare(that.sizeZ, sizeZ) == 0 &&
                Double.compare(that.speed, speed) == 0 &&
                force == that.force &&
                particle == that.particle &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(particle, count, sizeX, sizeY, sizeZ, speed, data, force);
    }
}
