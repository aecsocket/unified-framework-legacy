package me.aecsocket.unifiedframework.utils;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Field;

/** An {@link Enchantment} which can be applied to items to make them glowing. */
public class GlowEnchant extends Enchantment {
    public GlowEnchant(Plugin plugin) {
        super(new NamespacedKey(plugin, "glow"));
    }
    @SuppressWarnings("deprecation")
    @Override
    public String getName() { return ""; }
    @Override
    public int getMaxLevel() { return 0; }
    @Override
    public int getStartLevel() { return 0; }
    @Override
    public EnchantmentTarget getItemTarget() { return EnchantmentTarget.ALL; }
    @Override
    public boolean isTreasure() { return false; }
    @SuppressWarnings("deprecation")
    @Override
    public boolean isCursed() { return false; }
    @Override
    public boolean conflictsWith(Enchantment enchantment) { return false; }
    @Override
    public boolean canEnchantItem(ItemStack item) { return true; }

    public void initialize() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);

            Enchantment.registerEnchantment(this);

            f.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException ignore) {}
    }
}
