package me.aecsocket.unifiedframework.utils;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/** Stores information about playing {@link Sound}s in a world. */
public class SoundData implements Cloneable {
    private String sound;
    private SoundCategory category = SoundCategory.MASTER;
    private float volume = 1;
    private float pitch = 1;

    public SoundData(String sound, SoundCategory category, float volume, float pitch) {
        this.sound = sound;
        this.category = category;
        this.volume = volume;
        this.pitch = pitch;
    }

    public SoundData() {}

    /** Gets the sound as a string.
     * @return The sound as a string.
     */
    public String getSound() { return sound; }

    /** Sets the sound as a string.
     * @param sound The sound as a string.
     */
    public void setSound(String sound) { this.sound = sound; }

    /** Gets the {@link SoundCategory}.
     * @return The {@link SoundCategory}.
     */
    public SoundCategory getCategory() { return category; }

    /** Sets the {@link SoundCategory}.
     * @param category The {@link SoundCategory}.
     */
    public void setCategory(SoundCategory category) { this.category = category; }

    /** Gets the volume.
     * @return The volume.
     */
    public float getVolume() { return volume; }

    /** Sets the volume.
     * @param volume The volume.
     */
    public void setVolume(float volume) { this.volume = volume; }

    /** Gets the pitch.
     * @return The pitch.
     */
    public float getPitch() { return pitch; }

    /** Sets the pitch.
     * @param pitch The pitch.
     */
    public void setPitch(float pitch) { this.pitch = pitch; }

    /** Plays a sound in a world at a {@link Location}.
     * @param location The {@link Location}.
     */
    public void play(Location location) {
        if (location == null || location.getWorld() == null) return;
        for (Player player : location.getWorld().getPlayers())
            play(player, location);
    }

    /** Plays a sound to a {@link Player} at a {@link Location}.
     * @param player The {@link Player}.
     * @param location The {@link Location}.
     */
    public void play(@NotNull Player player, Location location) {
        player.playSound(location, sound, category, volume, pitch);
    }

    /** Plays an array of {@link SoundData} safely if anything is null.
     * @param data The {@link SoundData}.
     * @param player The {@link Player} to play to.
     * @param location The {@link Location}.
     */
    public static void play(SoundData[] data, Player player, Location location) {
        if (data != null) {
            for (SoundData sound : data) {
                if (sound != null)
                    sound.play(player, location);
            }
        }
    }

    /** Plays an array of {@link SoundData} safely if anything is null.
     * @param data The {@link SoundData}.
     * @param location The {@link Location}.
     */
    public static void play(SoundData[] data, Location location) {
        if (data != null) {
            for (SoundData sound : data) {
                if (sound != null)
                    sound.play(location);
            }
        }
    }

    public SoundData clone() { try { return (SoundData)super.clone(); } catch (CloneNotSupportedException ignore) { return null; } }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SoundData soundData = (SoundData)o;
        return Float.compare(soundData.volume, volume) == 0 &&
                Float.compare(soundData.pitch, pitch) == 0 &&
                sound.equals(soundData.sound) &&
                category == soundData.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sound, category, volume, pitch);
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
