package me.aecsocket.unifiedframework.utils;

import me.aecsocket.unifiedframework.locale.LocaleManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.*;

/** Utilities when running commands and tab completions */
public final class CommandUtils {
    private CommandUtils() {}

    /** Marker for selecting the player running the command. */
    public static final String THIS_PLAYER = ".";

    /** Gets a {@link List} of players to return for a tab completion.
     * @param token The text the sender has already typed.
     * @param ignore A {@link Collection} of {@link Player}s to not display.
     * @return A {@link List} of players to return for a tab completion.
     */
    public static List<String> getPlayers(String token, Collection<Player> ignore) {
        List<String> result = new ArrayList<>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (ignore == null || !ignore.contains(player))
                result.add(player.getName());
        }
        result.add(THIS_PLAYER);
        return StringUtil.copyPartialMatches(token, result, new ArrayList<>());
    }

    /** Gets a {@link List} of players to return for a tab completion.
     * @param token The text the sender has already typed.
     * @return A {@link List} of players to return for a tab completion.
     */
    public static List<String> getPlayers(String token) { return getPlayers(token, null); }

    /** Gets a {@link List} of players to return for a tab completion.
     * @return A {@link List} of players to return for a tab completion.
     */
    public static List<String> getPlayers() { return getPlayers(""); }

    /** Gets a {@link Player} which the sender has targeted. This method also gets players by the {@link CommandUtils#THIS_PLAYER} marker, and by UUID.
     * @param token The text the sender has typed.
     * @param sender The {@link CommandSender} which is sending this.
     * @return A {@link Player} which has been targeted, or null if none appropriate were found.
     */
    public static Player getTarget(String token, CommandSender sender) {
        // Try with this player
        if (token.equals(THIS_PLAYER))
            return (sender instanceof Player) ? (Player)sender : null;

        Player target;
        // Try with UUID
        try {
            UUID uuid = UUID.fromString(token);
            target = Bukkit.getPlayer(uuid);
        } catch (IllegalArgumentException ignore) {}
        // Try with name
        target = Bukkit.getPlayer(token);
        return target;
    }

    /** Gets all partial matches for a tab completion.
     * @param original The original {@link List} of results.
     * @param args The arguments entered in the tab completion.
     * @return The partial matches.
     */
    public static List<String> getPartialMatches(List<String> original, String[] args) {
        if (args.length > 0)
            return StringUtil.copyPartialMatches(args[args.length - 1], original, new ArrayList<>());
        else
            return original;
    }

    /** Gets a {@link CommandSender}'s locale if it is a player, or {@link LocaleManager#DEFAULT_LOCALE} otherwise.
     * @param sender The {@link CommandSender}.
     * @return The locale.
     */
    public static String getLocale(CommandSender sender) {
        return (sender instanceof Player) ? ((Player)sender).getLocale() : LocaleManager.DEFAULT_LOCALE;
    }

    /** Joins all arguments of a command after (inclusive) of an index.
     * @param args The arguments.
     * @param joinStart The index to start from.
     * @return The final string, or null if there were no arguments to join.
     */
    public static String joinArgsAfter(String[] args, int joinStart) {
        if (args.length < joinStart)
            return "";
        StringJoiner result = new StringJoiner(" ");
        for (int i = joinStart; i < args.length; i++)
            result.add(args[i]);
        return result.toString();
    }
}
