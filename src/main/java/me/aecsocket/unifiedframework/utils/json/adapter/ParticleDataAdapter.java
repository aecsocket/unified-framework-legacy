package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.Particle;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;

public class ParticleDataAdapter implements UtilJsonAdapter, JsonAdapter<ParticleData> {
    @Override
    public JsonElement serialize(ParticleData data, Type jType, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        object.addProperty("particle", data.getParticle().name());
        object.addProperty("count", data.getCount());
        if ((data.getSizeX() == data.getSizeY()) && (data.getSizeX() == data.getSizeZ()))
            object.addProperty("size", data.getSizeX());
        else {
            object.addProperty("size_x", data.getSizeX());
            object.addProperty("size_y", data.getSizeZ());
            object.addProperty("size_z", data.getSizeY());
        }
        object.addProperty("speed", data.getSpeed());
        if (data.getData() != null)
            object.add("data", context.serialize(data.getData()));
        if (data.isForced())
            object.addProperty("force", data.isForced());
        return object;
    }

    @Override
    public ParticleData deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = assertObject(json);
        Particle particle = getEnum(object, "particle", Particle.class);
        int count = get(object, "count").getAsInt();
        double sizeX = (object.has("size") ? object.get("size") : get(object, "size_x")).getAsDouble();
        double sizeY = (object.has("size") ? object.get("size") : get(object, "size_y")).getAsDouble();
        double sizeZ = (object.has("size") ? object.get("size") : get(object, "size_z")).getAsDouble();
        double speed = get(object, "speed").getAsDouble();
        Object data;
        switch (particle) {
            case REDSTONE:
                data = context.deserialize(JsonUtils.getOrDefault(object, "data", null), Particle.DustOptions.class);
                break;
            case ITEM_CRACK:
                data = context.deserialize(JsonUtils.getOrDefault(object, "data", null), ItemStack.class);
                break;
            case BLOCK_CRACK:
            case BLOCK_DUST:
            case FALLING_DUST:
                data = context.deserialize(JsonUtils.getOrDefault(object, "data", null), BlockData.class);
                break;
            default:
                data = getOrDefault(object, "data", null);
                break;
        }
        boolean force = getOrDefault(object, "force", new JsonPrimitive(false)).getAsBoolean();
        return new ParticleData(particle, count, sizeX, sizeY, sizeZ, speed, data, force);
    }
}
