package me.aecsocket.unifiedframework.utils.json;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/** Generic utilities for GSON. */
public final class JsonUtils {
    public static class ObjectBuilder {
        private JsonObject object = new JsonObject();

        public ObjectBuilder add(String property, JsonElement value) { object.add(property, value); return this; }
        public ObjectBuilder add(String property, Number value) { object.addProperty(property, value); return this; }
        public ObjectBuilder add(String property, String value) { object.addProperty(property, value); return this; }
        public ObjectBuilder add(String property, Boolean value) { object.addProperty(property, value); return this; }
        public ObjectBuilder add(String property, Character value) { object.addProperty(property, value); return this; }
        public JsonObject create() { return object; }
    }

    public static class ArrayBuilder {
        private JsonArray array = new JsonArray();

        public ArrayBuilder add(JsonElement value) { array.add(value); return this; }
        public ArrayBuilder add(Number value) { array.add(value); return this; }
        public ArrayBuilder add(String value) { array.add(value); return this; }
        public ArrayBuilder add(Boolean value) { array.add(value); return this; }
        public ArrayBuilder add(Character value) { array.add(value); return this; }
        public JsonArray create() { return array; }
    }

    private JsonUtils() {}

    /** Assert that the {@link JsonElement} is a {@link JsonObject}.
     * @param json The {@link JsonElement}.
     * @return The {@link JsonObject}.
     * @throws JsonParseException If the {@link JsonElement} is not a {@link JsonObject}.
     */
    public static JsonObject assertObject(JsonElement json) throws JsonParseException {
        if (json.isJsonObject())
            return json.getAsJsonObject();
        throw new JsonParseException("JSON element is not object");
    }

    /** Assert that the {@link JsonElement} is a {@link JsonArray}.
     * @param json The {@link JsonElement}.
     * @return The {@link JsonArray}.
     * @throws JsonParseException If the {@link JsonElement} is not a {@link JsonArray}.
     */
    public static JsonArray assertArray(JsonElement json) throws JsonParseException {
        if (json.isJsonArray())
            return json.getAsJsonArray();
        throw new JsonParseException("JSON element is not array");
    }

    /** Assert that the {@link JsonElement} is a {@link JsonPrimitive}.
     * @param json The {@link JsonElement}.
     * @return The {@link JsonPrimitive}.
     * @throws JsonParseException If the {@link JsonElement} is not a {@link JsonPrimitive}.
     */
    public static JsonPrimitive assertPrimitive(JsonElement json) throws JsonParseException {
        if (json.isJsonPrimitive())
            return json.getAsJsonPrimitive();
        throw new JsonParseException("JSON element is not primitive");
    }

    /** Assert that the {@link JsonElement} is a {@link JsonNull}.
     * @param json The {@link JsonElement}.
     * @return The {@link JsonNull}.
     * @throws JsonParseException If the {@link JsonElement} is not a {@link JsonNull}.
     */
    public static JsonNull assertNull(JsonElement json) throws JsonParseException {
        if (json.isJsonNull())
            return json.getAsJsonNull();
        throw new JsonParseException("JSON element is not null");
    }

    /** Gets an enum from a name with error handling.
     * @param name The name of the enum value.
     * @param clazz The class of the enum.
     * @param <T> The type of the enum.
     * @return The enum.
     * @throws JsonParseException If the enum name is invalid.
     */
    public static <T extends Enum<T>> T getEnum(String name, Class<T> clazz) throws JsonParseException {
        if (FrameworkUtils.isValidEnum(clazz, name))
            return T.valueOf(clazz, name);
        else
            throw new JsonParseException("Invalid " + clazz.getSimpleName() + " enum '" + name + "'");
    }

    /** Gets a {@link JsonElement} from a {@link JsonObject} member.
     * @param object The {@link JsonObject}.
     * @param member The member name.
     * @return The {@link JsonElement}.
     * @throws JsonParseException If the member does not exist.
     */
    public static JsonElement get(JsonObject object, String member) throws JsonParseException {
        if (object.has(member))
            return object.get(member);
        throw new JsonParseException("Member '" + member + "' has no value");
    }

    /** Gets a {@link JsonElement} from a {@link JsonObject} member, or a default if not found.
     * @param object The {@link JsonObject}.
     * @param member The member name.
     * @param defaultObject The default {@link JsonElement} if none is found.
     * @return The {@link JsonElement}.
     */
    public static JsonElement getOrDefault(JsonObject object, String member, JsonElement defaultObject) {
        if (object.has(member))
            return object.get(member);
        return defaultObject;
    }

    /** Gets an enum {@link T} from a {@link JsonObject} member.
     * @param object The {@link JsonObject}.
     * @param member The member name.
     * @param clazz The class of the enum.
     * @param <T> The type of the enum.
     * @return The enum.
     * @throws JsonParseException If the member does not exist or if the enum name is invalid.
     */
    public static <T extends Enum<T>> T getEnum(JsonObject object, String member, Class<T> clazz) throws JsonParseException {
        String name = get(object, member).getAsString();
        return getEnum(name, clazz);
    }

    /** Converts a {@link JsonArray} of one type to a {@link List}.
     * @param array The {@link JsonArray}.
     * @param type The {@link Type} of object.
     * @param context The {@link JsonDeserializationContext}.
     * @param <T> The {@link Type} of object.
     * @return The final {@link List}.
     */
    public static <T> List<T> arrayToRaw(JsonArray array, Type type, JsonDeserializationContext context) {
        List<T> list = new ArrayList<>();
        array.forEach(r -> list.add(context.deserialize(r, type)));
        return list;
    }

    public static ObjectBuilder buildObject() { return new ObjectBuilder(); }
    public static ArrayBuilder buildArray() { return new ArrayBuilder(); }
}
