package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.gui.core.GUIVector;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.Type;

public class GUIVectorAdapter implements UtilJsonAdapter, JsonAdapter<GUIVector> {
    @Override
    public JsonElement serialize(GUIVector vector, Type jType, JsonSerializationContext context) {
        return buildArray()
                .add(vector.getX())
                .add(vector.getY())
                .create();
    }

    @Override
    public GUIVector deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        return new GUIVector(
                array.get(0).getAsInt(),
                array.get(1).getAsInt()
        );
    }
}
