package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Type;
import java.util.Objects;

public class PotionEffectAdapter implements UtilJsonAdapter, JsonAdapter<PotionEffect> {
    @Override
    public JsonElement serialize(PotionEffect effect, Type jType, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        object.add("type", new JsonPrimitive(effect.getType().getName()));
        object.add("duration", new JsonPrimitive(effect.getDuration()));
        if (effect.getAmplifier() != 0) object.add("amplifier", new JsonPrimitive(effect.getAmplifier()));
        if (effect.isAmbient()) object.add("ambient", new JsonPrimitive(true));
        if (effect.hasParticles()) object.add("particles", new JsonPrimitive(true));
        if (effect.hasIcon()) object.add("icon", new JsonPrimitive(true));
        return object;
    }

    @Override
    public PotionEffect deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = assertObject(json);
        return new PotionEffect(
                Objects.requireNonNull(PotionEffectType.getByName(object.get("type").getAsString())),
                object.get("duration").getAsInt(),
                getOrDefault(object, "amplifier", new JsonPrimitive(0)).getAsInt(),
                getOrDefault(object, "ambient", new JsonPrimitive(false)).getAsBoolean(),
                getOrDefault(object, "particles", new JsonPrimitive(false)).getAsBoolean(),
                getOrDefault(object, "icon", new JsonPrimitive(false)).getAsBoolean()
        );
    }
}
