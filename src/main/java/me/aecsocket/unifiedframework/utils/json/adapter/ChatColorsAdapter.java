package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.ChatColors;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.ChatColor;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ChatColorsAdapter implements UtilJsonAdapter, JsonAdapter<ChatColors> {
    @Override
    public JsonElement serialize(ChatColors colors, Type jType, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        for (ChatColor color : colors.getColors())
            array.add(color.name());
        return array;
    }

    @Override
    public ChatColors deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        List<ChatColor> colors = new ArrayList<>();
        for (JsonElement element : array) {
            JsonPrimitive primitive = assertPrimitive(element);
            colors.add(JsonUtils.getEnum(primitive.getAsString(), ChatColor.class));
        }
        return new ChatColors(colors.toArray(new ChatColor[0]));
    }
}
