package me.aecsocket.unifiedframework.utils.json;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;

/** An interface combining a {@link JsonSerializer} and {@link JsonDeserializer}.
 * @param <T> The type to (de)serialize to.
 */
public interface JsonAdapter<T> extends JsonSerializer<T>, JsonDeserializer<T> { }
