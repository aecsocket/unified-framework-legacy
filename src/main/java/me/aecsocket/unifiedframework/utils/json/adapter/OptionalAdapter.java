package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

public class OptionalAdapter<T> implements UtilJsonAdapter, JsonAdapter<Optional<T>> {
    @Override
    public JsonElement serialize(Optional<T> optional, Type jType, JsonSerializationContext context) {
        if (optional.isPresent())
            return context.serialize(optional.get());
        else
            return new JsonArray();
    }

    @Override
    public Optional<T> deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        Type type = ((ParameterizedType)jType).getActualTypeArguments()[0];
        if (json.isJsonArray()) {
            JsonArray array = json.getAsJsonArray();
            if (array.size() == 0)
                return Optional.empty();
            return Optional.of(context.deserialize(array.get(0), type));
        } else
            return Optional.of(context.deserialize(json, type));
    }
}
