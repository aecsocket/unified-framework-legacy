package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.util.Vector;

import java.lang.reflect.Type;

public class VectorAdapter implements UtilJsonAdapter, JsonAdapter<Vector> {
    @Override
    public JsonElement serialize(Vector vector, Type jType, JsonSerializationContext context) {
        return buildArray()
                .add(vector.getX())
                .add(vector.getY())
                .add(vector.getZ())
                .create();
    }

    @Override
    public Vector deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        return new Vector(
                array.get(0).getAsDouble(),
                array.get(1).getAsDouble(),
                array.get(2).getAsDouble()
        );
    }
}
