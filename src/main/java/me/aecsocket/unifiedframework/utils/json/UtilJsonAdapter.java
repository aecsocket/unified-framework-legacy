package me.aecsocket.unifiedframework.utils.json;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.List;

public interface UtilJsonAdapter {
    default JsonElement get(JsonObject json, String member) { return JsonUtils.get(json, member); }
    default JsonElement getOrDefault(JsonObject json, String member, JsonElement defaultObject) { return JsonUtils.getOrDefault(json, member, defaultObject); }
    default <O> O get(JsonObject json, String member, Type type, JsonDeserializationContext context) { return context.deserialize(get(json, member), type); }
    default <O> O get(JsonObject json, String member, Type type, JsonDeserializationContext context, O defaultObject) { return json.has(member) ? context.deserialize(get(json, member), type) : defaultObject; }
    default <O extends Enum<O>> O getEnum(JsonObject json, String member, Class<O> clazz) { return JsonUtils.getEnum(json, member, clazz); }

    default JsonObject assertObject(JsonElement json) { return JsonUtils.assertObject(json); }
    default JsonArray assertArray(JsonElement json) { return JsonUtils.assertArray(json); }
    default JsonPrimitive assertPrimitive(JsonElement json) { return JsonUtils.assertPrimitive(json); }
    default JsonNull assertNull(JsonElement json) { return JsonUtils.assertNull(json); }

    default <O> List<O> fromArray(JsonObject object, String member, Type type, JsonDeserializationContext context) {
        return JsonUtils.arrayToRaw(assertArray(get(object, member)), type, context);
    }

    default JsonUtils.ObjectBuilder buildObject() { return JsonUtils.buildObject(); }
    default JsonUtils.ArrayBuilder buildArray() { return JsonUtils.buildArray(); }
}
