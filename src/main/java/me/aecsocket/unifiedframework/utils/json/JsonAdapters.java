package me.aecsocket.unifiedframework.utils.json;

import me.aecsocket.unifiedframework.utils.json.adapter.*;

public final class JsonAdapters {
    private JsonAdapters() {}

    public static final BoundingBoxAdapter BOUNDING_BOX = new BoundingBoxAdapter();
    public static final ChatColorsAdapter CHAT_COLORS = new ChatColorsAdapter();
    public static final GUIVectorAdapter GUI_VECTOR = new GUIVectorAdapter();
    public static final LocationAdapter LOCATION = new LocationAdapter();
    public static final OptionalAdapter<?> OPTIONAL = new OptionalAdapter<>();
    public static final ParticleDataAdapter PARTICLE_DATA = new ParticleDataAdapter();
    public static final PotionEffectAdapter POTION_EFFECT = new PotionEffectAdapter();
    public static final VectorAdapter VECTOR = new VectorAdapter();
    public static final Vector2Adapter VECTOR2 = new Vector2Adapter();
}
