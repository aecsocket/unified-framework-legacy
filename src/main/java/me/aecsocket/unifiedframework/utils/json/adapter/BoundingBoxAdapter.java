package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.util.BoundingBox;

import java.lang.reflect.Type;

public class BoundingBoxAdapter implements UtilJsonAdapter, JsonAdapter<BoundingBox> {
    @Override
    public JsonElement serialize(BoundingBox box, Type jType, JsonSerializationContext context) {
        return buildArray()
                .add(box.getMinX())
                .add(box.getMinY())
                .add(box.getMinZ())

                .add(box.getMaxX())
                .add(box.getMaxY())
                .add(box.getMaxZ())
                .create();
    }

    @Override
    public BoundingBox deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        return new BoundingBox(
                array.get(0).getAsDouble(),
                array.get(1).getAsDouble(),
                array.get(2).getAsDouble(),

                array.get(3).getAsDouble(),
                array.get(4).getAsDouble(),
                array.get(5).getAsDouble()
        );
    }
}
