package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.lang.reflect.Type;

public class LocationAdapter implements UtilJsonAdapter, JsonAdapter<Location> {
    @Override
    public JsonElement serialize(Location location, Type jType, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        World world = location.getWorld();
        array.add(world == null ? null : world.getName());
        array.add(location.getX());
        array.add(location.getY());
        array.add(location.getZ());
        if (location.getYaw() != 0 || location.getPitch() != 0) {
            array.add(location.getYaw());
            array.add(location.getPitch());
        }
        return array;
    }

    @Override
    public Location deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        return new Location(
                array.get(0).isJsonPrimitive() ? Bukkit.getWorld(array.get(0).getAsString()) : null,
                array.get(1).getAsDouble(),
                array.get(2).getAsDouble(),
                array.get(3).getAsDouble(),
                array.size() > 4 ? array.get(4).getAsFloat() : 0,
                array.size() > 5 ? array.get(5).getAsFloat() : 0);
    }
}
