package me.aecsocket.unifiedframework.utils.json.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.Vector2;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.Type;

public class Vector2Adapter implements UtilJsonAdapter, JsonAdapter<Vector2> {
    @Override
    public JsonElement serialize(Vector2 vector, Type jType, JsonSerializationContext context) {
        return buildArray()
                .add(vector.getX())
                .add(vector.getY())
                .create();
    }

    @Override
    public Vector2 deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        return new Vector2(
                array.get(0).getAsDouble(),
                array.get(1).getAsDouble()
        );
    }
}
