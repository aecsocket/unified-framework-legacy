package me.aecsocket.unifiedframework.utils;

import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** A wrapper for {@link Scoreboard} allowing custom text to be displayed. */
public class TextScoreboard extends Wrapper<Scoreboard> {
    /** The maximum size of the scoreboard. */
    public static final int MAX_SIZE = 16;

    private List<String> text;

    private Objective objective;
    private List<String> oldScores;

    public TextScoreboard(Scoreboard handle, List<String> text) {
        super(handle);
        this.text = text;

        this.objective = handle.registerNewObjective("objective", "dummy", " ");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.oldScores = null;
    }

    public TextScoreboard(Scoreboard scoreboard, Collection<String> text) {
        this(scoreboard, new ArrayList<>(text));
    }

    public TextScoreboard(Scoreboard scoreboard) {
        this(scoreboard, new ArrayList<>());
    }

    /** Gets a {@link List} of text to be displayed.
     * @return A {@link List} of text to be displayed.
     */
    public List<String> getText() { return text; }

    /** Sets the {@link List} of text to be displayed.
     * @param text The {@link List} of text to be displayed.
     */
    public void setText(List<String> text) { this.text = text; }

    /** Gets the dummy scoreboard {@link Objective}.
     * @return The dummy scoreboard {@link Objective}.
     */
    public Objective getObjective() { return objective; }

    /** Sets the header of the scoreboard sidebar.
     * @param name The header.
     */
    public void setHeader(String name) { objective.setDisplayName(name); }

    /** Updates all scores, removing old scores. */
    public void update() {
        if (oldScores == null)
            oldScores = new ArrayList<>();

        for (String score : oldScores) {
            if (!text.contains(score))
                getHandle().resetScores(score);
        }

        oldScores.clear();
        for (int i = 0; i < Math.min(MAX_SIZE, text.size()); i++) {
            String line = text.get(i);
            objective.getScore(line).setScore(MAX_SIZE - i - 1);
            oldScores.add(line);
        }
    }
}
