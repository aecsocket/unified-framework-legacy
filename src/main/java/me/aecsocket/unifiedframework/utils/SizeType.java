package me.aecsocket.unifiedframework.utils;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/** The size of an {@link Inventory}, determined by an {@link InventoryType} or number of rows. */
public class SizeType {
    private InventoryType type;
    private int rows;

    /** Creates an instance with an {@link InventoryType}.
     * @param type The {@link InventoryType}.
     */
    public SizeType(InventoryType type) {
        this.type = type;
        this.rows = -1;
    }

    /** Creates an instance with a number of rows.
     * @param rows The number of rows.
     */
    public SizeType(int rows) {
        this.type = null;
        this.rows = Math.max(1, Math.min(6, rows));
    }

    /** If this instance contains a number of rows (true) or an {@link InventoryType} (false).
     * @return If this instance contains a number of rows (true) or an {@link InventoryType} (false).
     */
    public boolean isRows() { return rows != -1; }

    /** Gets the {@link InventoryType}.
     * @return The {@link InventoryType}.
     */
    public InventoryType getType() { return type; }

    /** Gets the number of rows.
     * @return The number of rows.
     */
    public int getRows() { return rows; }

    /** Creates an {@link Inventory} from the data given.
     * @param owner The {@link InventoryHolder} owner.
     * @param title The {@link String} title.
     * @return The resulting {@link Inventory}.
     */
    public Inventory createInventory(InventoryHolder owner, String title) {
        if (isRows())
            return Bukkit.createInventory(owner, rows * 9, title);
        else
            return Bukkit.createInventory(owner, type, title);
    }
}
