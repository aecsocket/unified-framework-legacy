package me.aecsocket.unifiedframework;

import me.aecsocket.unifiedframework.gui.GUIManager;
import me.aecsocket.unifiedframework.item.ItemManager;
import me.aecsocket.unifiedframework.locale.LocaleManager;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import me.aecsocket.unifiedframework.npc.NPCManager;
import me.aecsocket.unifiedframework.resource.ResourceManager;
import me.aecsocket.unifiedframework.utils.GlowEnchant;
import me.aecsocket.unifiedframework.utils.PlayerTasks;
import me.aecsocket.unifiedframework.utils.event.PlayerJumpEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

/** The main plugin class. */
public class UnifiedFramework implements Tickable {
    /** The metadata value of a {@link Player} determining their last position. */
    public static final String LAST_LOC_META = "unifiedframework_lastLoc";
    /** The metadata value of a {@link Player} determining their velocity as a vector. */
    public static final String VELOCITY_META = "unifiedframework_velocity";
    /** The metadata value of a {@link Player} determining their current speed. */
    public static final String SPEED_META = "unifiedframework_speed";
    /** The metadata value of a {@link Player} determining how many jumps a player had in the previous tick. */
    public static final String JUMPS_META = "unifiedframework_meta";

    private final Plugin plugin;
    private final ResourceManager resourceManager;
    private final LocaleManager localeManager;
    private final GUIManager guiManager;
    private final ItemManager itemManager;
    private final NPCManager npcManager;
    private final GlowEnchant glowEnchant;

    public UnifiedFramework(Plugin plugin) {
        this.plugin = plugin;
        this.resourceManager = new ResourceManager();
        this.localeManager = new LocaleManager();
        this.guiManager = new GUIManager(plugin);
        this.itemManager = new ItemManager(plugin);
        this.npcManager = new NPCManager(plugin);
        this.glowEnchant = new GlowEnchant(plugin);
        glowEnchant.initialize();
        plugin.getServer().getPluginManager().registerEvents(new EventHandle(), plugin);
    }

    public void disable() {
        guiManager.getViews().forEach((r, s) -> r.close());
        npcManager.getNPCs().forEach(r -> {
            if (r.getHandle() != null)
                r.getHandle().remove();
        });
    }

    /** Gets the {@link ResourceManager}.
     * @return The {@link ResourceManager}.
     */
    public ResourceManager getResourceManager() { return resourceManager; }

    /** Gets the {@link LocaleManager}.
     * @return The {@link LocaleManager}.
     */
    public LocaleManager getLocaleManager() { return localeManager; }

    /** Gets the {@link GUIManager}.
     * @return The {@link GUIManager}.
     */
    public GUIManager getGUIManager() { return guiManager; }

    /** Gets the {@link ItemManager}.
     * @return The {@link ItemManager}.
     */
    public ItemManager getItemManager() { return itemManager; }

    /** Gets the {@link NPCManager}.
     * @return The {@link NPCManager}.
     */
    public NPCManager getNPCManager() { return npcManager; }

    /** Gets the {@link GlowEnchant}.
     * @return The {@link GlowEnchant}.
     */
    public GlowEnchant getGlowEnchant() { return glowEnchant;}

    @Override
    public void tick(TickContext context) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            // Calculate speed
            if (player.hasMetadata(LAST_LOC_META)) {
                String[] lastCoords = player.getMetadata(LAST_LOC_META).get(0).asString().split(",");
                Vector lastLoc = new Vector(
                        Double.parseDouble(lastCoords[0]),
                        Double.parseDouble(lastCoords[1]),
                        Double.parseDouble(lastCoords[2]));
                Vector delta = player.getLocation().toVector().subtract(lastLoc);
                player.setMetadata(VELOCITY_META, new FixedMetadataValue(plugin, delta.getX() + "," + delta.getY() + "," + delta.getZ()));
                double speed = delta.length();
                player.setMetadata(SPEED_META, new FixedMetadataValue(plugin, speed));
            }
            Location loc = player.getLocation();
            player.setMetadata(LAST_LOC_META, new FixedMetadataValue(plugin, loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch()));

            // Calculate jump
            int jumps = player.getStatistic(Statistic.JUMP);
            if (player.hasMetadata(JUMPS_META)) {
                int lastJumps = player.getMetadata(JUMPS_META).get(0).asInt();

                if (jumps > lastJumps)
                    plugin.getServer().getPluginManager().callEvent(new PlayerJumpEvent(player));
            }
            player.setMetadata(JUMPS_META, new FixedMetadataValue(plugin, jumps));

            // Update tasks
            PlayerTasks.updateTasks(player);
        }
    }
}
