package me.aecsocket.unifiedframework.gui;

import me.aecsocket.unifiedframework.gui.core.GUI;
import me.aecsocket.unifiedframework.gui.core.GUIItem;
import me.aecsocket.unifiedframework.gui.core.GUIView;
import me.aecsocket.unifiedframework.utils.SizeType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

import java.util.Map;

/** A helper wrapper around {@link GUI}. */
public abstract class BaseGUI implements GUI {
    /** Gets the title of the {@link Inventory}.
     * @param player The {@link Player} to show this to.
     * @return The {@link String} title.
     */
    public abstract String getTitle(Player player);

    /** Gets the {@link SizeType} of the {@link Inventory}.
     * @param player The {@link Player} to show this to.
     * @return The {@link SizeType}.
     */
    public abstract SizeType getSizeType(Player player);

    /** Gets a {@link Map} of all slots of the {@link Inventory}.
     * @param player The {@link Player} to show this to.
     * @return The {@link Map} of all slots.
     */
    public abstract Map<Integer, GUIItem> getSlots(Player player);

    protected abstract GUIManager getGUIManager();

    @Override
    public GUIView open(Player player) {
        Inventory inv = getSizeType(player).createInventory(player, getTitle(player));
        InventoryView inventoryView = player.openInventory(inv);

        GUIView view = new GUIView(inventoryView, this, getSlots(player));
        getGUIManager().getViews().put(inventoryView, view);

        return view;
    }

    @Override
    public void onClick(GUIView view, InventoryClickEvent event) {
        InventoryView handle = view.getHandle();
        int slot = event.getSlot();
        if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)
            event.setCancelled(true);
        else if (event.getClickedInventory() == handle.getTopInventory()) {
            event.setCancelled(true);
            view.clickSlot(slot, event);
        } else {
            if (view.getCursor() != null)
                view.getCursor().onMoveToOtherInventory(view, event);
        }
    }

    @Override
    public void onDrag(GUIView view, InventoryDragEvent event) {
        if (event.getInventory() == event.getView().getTopInventory())
            event.setCancelled(true);
    }

    @Override
    public void onClose(GUIView view, InventoryCloseEvent event) {
        event.getView().setCursor(null);
    }
}
