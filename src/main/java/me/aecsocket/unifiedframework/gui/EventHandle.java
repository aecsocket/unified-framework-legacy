package me.aecsocket.unifiedframework.gui;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.InventoryView;

/** {@link GUIManager}'s event {@link Listener}. */
public class EventHandle implements Listener {
    private GUIManager manager;

    public EventHandle(GUIManager manager) {
        this.manager = manager;
    }

    public GUIManager getManager() { return manager; }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onClose(InventoryCloseEvent event) {
        InventoryView view = event.getView();
        if (manager.getViews().containsKey(view)) {
            manager.getViews().get(view).onClose(event);
            manager.getViews().remove(event.getView());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onClick(InventoryClickEvent event) {
        InventoryView view = event.getView();
        if (manager.getViews().containsKey(view))
            manager.getViews().get(view).onClick(event);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDrag(InventoryDragEvent event) {
        InventoryView view = event.getView();
        if (manager.getViews().containsKey(view))
            manager.getViews().get(view).onDrag(event);
    }
}
