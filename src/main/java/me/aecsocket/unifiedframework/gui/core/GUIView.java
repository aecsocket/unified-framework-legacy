package me.aecsocket.unifiedframework.gui.core;

import me.aecsocket.unifiedframework.utils.Wrapper;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

import java.util.HashMap;
import java.util.Map;

/** A wrapper around {@link InventoryView} which contains extra data relating to {@link GUI}s. */
public class GUIView extends Wrapper<InventoryView> {
    private final GUI gui;
    private Map<Integer, GUIItem> slots;
    private GUIItem cursor;

    public GUIView(InventoryView handle, GUI gui, Map<Integer, GUIItem> slots, GUIItem cursor) {
        super(handle);
        this.gui = gui;
        this.slots = slots;
        this.cursor = cursor;
        updateInventory();
    }

    public GUIView(InventoryView view, GUI gui, Map<Integer, GUIItem> slots) {
        this(view, gui, slots, null);
    }

    public GUIView(InventoryView view, GUI gui) {
        this(view, gui, new HashMap<>());
    }

    /** Updates all slots to be properly mapped to their handles. */
    public void updateInventory() {
        Inventory inv = getHandle().getTopInventory();
        inv.clear();
        for (Map.Entry<Integer, GUIItem> pair : slots.entrySet()) {
            int slot = pair.getKey();
            if (slot < inv.getSize())
                inv.setItem(slot, pair.getValue().getHandle(this));
        }
        if (cursor != null)
            getHandle().setCursor(cursor.getHandle(this));
    }

    /** Gets the base {@link GUI}.
     * @return The base {@link GUI}.
     */
    public GUI getGUI() { return gui; }

    /** Gets a {@link Map} of integer slots linked to {@link GUIItem}s in those slots.
     * @return A {@link Map} of integer slots linked to {@link GUIItem}s in those slots.
     */
    public Map<Integer, GUIItem> getSlots() { return slots; }

    public GUIItem getCursor() { return cursor; }
    public void setCursor(GUIItem cursor) {
        this.cursor = cursor;
        //getHandle().setCursor(cursor == null ? null : cursor.getHandle(this));
    }

    /** Helper method to get the {@link Player} of the handle.
     * @return The {@link Player} of the handle.
     */
    public Player getPlayer() {
        HumanEntity entity = getHandle().getPlayer();
        return entity instanceof Player ? (Player)entity : null;
    }

    public void onClick(InventoryClickEvent event) { gui.onClick(this, event); }
    public void onDrag(InventoryDragEvent event) { gui.onDrag(this, event); }
    public void onClose(InventoryCloseEvent event) { gui.onClose(this, event); }

    /** Runs when a slot is clicked in an {@link InventoryClickEvent}.
     * @param slot The clicked slot.
     * @param event The {@link InventoryClickEvent}.
     */
    public void clickSlot(int slot, InventoryClickEvent event) {
        if (cursor != null) {
            if (slots.containsKey(slot)) {
                cursor.onRelease(this, event, slots.get(slot));
                if (!event.isCancelled()) {
                    slots.put(slot, cursor);
                    setCursor(slots.get(slot));
                }
            } else {
                cursor.onRelease(this, event, null);
                if (!event.isCancelled()) {
                    slots.put(slot, cursor);
                    setCursor(null);
                }
            }
            return;
        }
        if (slots.containsKey(slot)) {
            GUIItem item = slots.get(slot);
            item.onClick(this, event);
            if (!event.isCancelled()) {
                setCursor(item);
                slots.remove(slot);
            }
        }
    }
}
