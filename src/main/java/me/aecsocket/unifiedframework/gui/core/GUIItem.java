package me.aecsocket.unifiedframework.gui.core;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/** A clickable item shown in a {@link GUIView}. */
public interface GUIItem {
    /** Gets the {@link ItemStack} representation of this instance.
     * @param view The {@link GUIView} this instance is shown in.
     * @return The {@link ItemStack} representation of this instance.
     */
    ItemStack getHandle(GUIView view);

    /** Runs when this instance is clicked in a {@link GUIView}.
     * @param view The {@link GUIView}.
     * @param event The {@link InventoryClickEvent} this was clicked from.
     */
    default void onClick(GUIView view, InventoryClickEvent event) {}

    /** Runs when this instance is placed down in a {@link GUIView}.
     * @param view The {@link GUIView}.
     * @param event The {@link InventoryClickEvent} this was placed down from.
     * @param existingItem The {@link GUIItem} that was already in that slot, or null if there was none.
     */
    default void onRelease(GUIView view, InventoryClickEvent event, GUIItem existingItem) {}

    /** Runs when this instance is moved to a {@link GUIView}'s bottom inventory.
     * @param view The {@link GUIView}.
     * @param event The {@link InventoryClickEvent} this was clicked from.
     */
    default void onMoveToOtherInventory(GUIView view, InventoryClickEvent event) {}
}
