package me.aecsocket.unifiedframework.gui.core;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

/** A wrapper around an {@link org.bukkit.inventory.Inventory} that opens for {@link Player}s. */
public interface GUI {
    /** Opens a GUI for a {@link Player}.
     * @param player The {@link Player}.
     * @return The {@link GUIView} of the {@link Player}.
     */
    GUIView open(Player player);

    void onClick(GUIView view, InventoryClickEvent event);
    void onDrag(GUIView view, InventoryDragEvent event);
    void onClose(GUIView view, InventoryCloseEvent event);
}
