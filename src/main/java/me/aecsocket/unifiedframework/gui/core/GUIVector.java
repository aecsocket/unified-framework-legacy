package me.aecsocket.unifiedframework.gui.core;

import java.util.Objects;

/** Represents a location in an inventory, with an x and y. */
public class GUIVector implements Cloneable {
    private int x;
    private int y;

    public GUIVector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public GUIVector() { this(0, 0); }

    public int getX() { return x; }
    public void setX(int x) { this.x = x; }

    public int getY() { return y; }
    public void setY(int y) { this.y = y; }

    public int getSlot() { return of(x, y); }

    public GUIVector add(GUIVector other) {
        if (other == null)
            return this;
        x += other.x;
        y += other.y;
        return this;
    }

    public GUIVector clone() { try { return (GUIVector)super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    public static int of(int x, int y) { return (y * 9) + x; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GUIVector guiVector = (GUIVector)o;
        return x == guiVector.x &&
                y == guiVector.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() { return "(" + x + ", " + y + ")"; }
}
