package me.aecsocket.unifiedframework.gui;

import me.aecsocket.unifiedframework.gui.core.GUIView;
import org.bukkit.Bukkit;
import org.bukkit.inventory.InventoryView;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

/** Manages open inventory menus which have clickable items. */
public class GUIManager {
    private final Map<InventoryView, GUIView> views;

    public GUIManager(Plugin plugin) {
        this.views = new HashMap<>();
        Bukkit.getPluginManager().registerEvents(new EventHandle(this), plugin);
    }

    /** Gets a {@link Map} of all active {@link GUIView}s linked to their {@link InventoryView} handles.
     * @return A {@link Map} of all active {@link GUIView}s linked to their {@link InventoryView} handles.
     */
    public Map<InventoryView, GUIView> getViews() { return views; }
}
