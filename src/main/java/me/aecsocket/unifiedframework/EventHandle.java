package me.aecsocket.unifiedframework;

import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import me.aecsocket.unifiedframework.utils.PlayerTasks;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventHandle implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        FrameworkUtils.cleanPlayer(player);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        PlayerTasks.cancelTasks(player);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDrop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        FrameworkUtils.LAST_DROP_TIME.put(player, Bukkit.getCurrentTick());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onClick(InventoryClickEvent event) {
        Player player = (Player)event.getWhoClicked();
        FrameworkUtils.LAST_CLICK_TIME.put(player, Bukkit.getCurrentTick());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onClose(InventoryCloseEvent event) {
        Player player = (Player)event.getPlayer();
        FrameworkUtils.LAST_CLICK_TIME.put(player, Bukkit.getCurrentTick());
    }
}
