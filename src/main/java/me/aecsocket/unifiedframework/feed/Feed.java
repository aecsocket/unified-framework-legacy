package me.aecsocket.unifiedframework.feed;

import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.*;

/** An auto-expiring feed which is displayed as a {@link BossBar} to players. */
public class Feed implements Tickable {
    /** The default maximum amount of entries in a feed. */
    public static final int DEFAULT_MAX_SIZE = 5;
    /** The default {@link BarColor}. */
    public static final BarColor BAR_COLOR = BarColor.WHITE;
    /** The default {@link BarStyle}. */
    public static final BarStyle BAR_STYLE = BarStyle.SOLID;
    /** The default bar progress. */
    public static final double BAR_PROGRESS = 0.0d;

    private List<Map.Entry<FeedEntry, BossBar>> entries;
    private List<Player> players;
    private int maxSize;

    public Feed(List<Map.Entry<FeedEntry, BossBar>> entries, List<Player> players, int maxSize) {
        this.entries = entries;
        this.players = players;
        this.maxSize = maxSize;
    }

    public Feed(int maxSize) {
        this(new ArrayList<>(), new ArrayList<>(), maxSize);
    }

    public Feed() {
        this(DEFAULT_MAX_SIZE);
    }

    public void destroy() {
        entries.forEach(r -> r.getValue().removeAll());
    }

    /** Return a {@link Map} of {@link Map.Entry} objects corresponding to {@link FeedEntry} objects and {@link BossBar}s.
     * @return A {@link Map} of {@link Map.Entry} objects corresponding to {@link FeedEntry} objects and {@link BossBar}s.
     */
    public List<Map.Entry<FeedEntry, BossBar>> getEntries() { return new ArrayList<>(entries); }

    /** Adds a {@link FeedEntry}.
     * @param entry The {@link FeedEntry}.
     * @return The resulting {@link BossBar}.
     */
    public BossBar addEntry(FeedEntry entry) {
        BossBar bar = Bukkit.createBossBar(entry.getText(), BAR_COLOR, BAR_STYLE);
        bar.setProgress(BAR_PROGRESS);
        players.forEach(bar::addPlayer);
        entries.add(new AbstractMap.SimpleEntry<>(entry, bar));
        if (entries.size() > maxSize)
            removeEntry(0);
        return bar;
    }

    /** Removes a {@link FeedEntry}.
     * @param entry The {@link FeedEntry}.
     */
    public void removeEntry(FeedEntry entry) {
        Iterator<Map.Entry<FeedEntry, BossBar>> iter = entries.iterator();
        while (iter.hasNext()) {
            Map.Entry<FeedEntry, BossBar> pair = iter.next();
            if (pair.getKey() == entry) {
                pair.getValue().removeAll();
                iter.remove();
            }
        }
    }

    /** Removes a {@link FeedEntry} by index.
     * @param index The index.
     */
    public void removeEntry(int index) {
        removeEntry(entries.get(index).getKey());
    }

    /** Gets the {@link List} of {@link Player}s registered to this instance.
     * @return The {@link List} of {@link Player}s registered to this instance.
     */
    public List<Player> getPlayers() { return players; }

    /** Gets the maximum amount of entries on this instance.
     * @return The maximum amount of entries on this instance.
     */
    public int getMaxSize() { return maxSize; }

    /** Sets the maximum amount of entries on this instance.
     * @param maxSize The maximum amount of entries on this instance.
     */
    public void setMaxSize(int maxSize) { this.maxSize = maxSize; }

    @Override
    public void tick(TickContext context) {
        Iterator<Map.Entry<FeedEntry, BossBar>> iter = entries.iterator();
        while (iter.hasNext()) {
            Map.Entry<FeedEntry, BossBar> pair = iter.next();
            if (Bukkit.getCurrentTick() >= pair.getKey().getExpireTime()) {
                pair.getValue().removeAll();
                iter.remove();
            }
        }
    }
}
