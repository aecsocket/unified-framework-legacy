package me.aecsocket.unifiedframework.feed;

/** An entry in a {@link Feed}. */
public class FeedEntry {
    private String text;
    private int expireTime;

    public FeedEntry(String text, int expireTime) {
        this.text = text;
        this.expireTime = expireTime;
    }

    /** Gets the {@link org.bukkit.boss.BossBar} text.
     * @return The {@link org.bukkit.boss.BossBar} text.
     */
    public String getText() { return text; }

    /** Sets the {@link org.bukkit.boss.BossBar} text.
     * @param text The {@link org.bukkit.boss.BossBar} text.
     */
    public void setText(String text) { this.text = text; }

    /** Gets the expire time.
     * @return The expire time.
     */
    public int getExpireTime() { return expireTime; }

    /** Sets the expire time.
     * @param expireTime The expire time.
     */
    public void setExpireTime(int expireTime) { this.expireTime = expireTime; }
}
