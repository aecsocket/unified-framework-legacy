package me.aecsocket.unifiedframework.exception;

public class IncompatibleComponentException extends RuntimeException {
    public IncompatibleComponentException() {}
    public IncompatibleComponentException(String message) { super(message); }
    public IncompatibleComponentException(String message, Throwable cause) { super(message, cause); }
    public IncompatibleComponentException(Throwable cause) { super(cause); }
    public IncompatibleComponentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) { super(message, cause, enableSuppression, writableStackTrace); }
}
