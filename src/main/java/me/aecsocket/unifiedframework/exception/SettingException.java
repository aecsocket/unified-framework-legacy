package me.aecsocket.unifiedframework.exception;

public class SettingException extends RuntimeException {
    public SettingException() {}
    public SettingException(String message) { super(message); }
    public SettingException(String message, Throwable cause) { super(message, cause); }
    public SettingException(Throwable cause) { super(cause); }
    public SettingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) { super(message, cause, enableSuppression, writableStackTrace); }
}
