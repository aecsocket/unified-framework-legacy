package me.aecsocket.unifiedframework.stat;

public interface Operation {
    String getSymbol();
}
