package me.aecsocket.unifiedframework.stat;

import com.google.gson.reflect.TypeToken;

import java.util.Objects;

public class StatInstance<T> implements Cloneable {
    private Stat<T> stat;
    private T value;
    private Operation op;

    public StatInstance(Stat<T> stat, T value, Operation op) {
        this.stat = stat;
        this.value = value;
        this.op = op;
    }

    public Stat<T> getStat() { return stat; }
    public void setStat(Stat<T> stat) { this.stat = stat; }

    public T getValue() { return value == null ? stat.getDefaultSupplier() == null ? null : stat.getDefaultSupplier().apply(this) : value; }
    public T getRawValue() { return value; }
    public void setValue(T value) { this.value = value; }

    public Operation getOp() { return op; }
    public void setOp(Operation op) { this.op = op; }

    public T combine(T other) { return stat.combine(value, other, op); }
    public T combine(T other, Operation op) { return stat.combine(value, other, op); }

    @SuppressWarnings("unchecked") public StatInstance<T> clone() { try { return (StatInstance<T>)super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    @Override
    public String toString() { return "[" + TypeToken.get(stat.getType()).getRawType().getSimpleName() + "] " + (op == null ? "" : op.getSymbol()) + value; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatInstance<?> that = (StatInstance<?>)o;
        return stat.equals(that.stat) &&
                Objects.equals(value, that.value) &&
                Objects.equals(op, that.op);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stat, value, op);
    }
}
