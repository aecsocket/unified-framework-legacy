package me.aecsocket.unifiedframework.stat;

import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public interface Stat<T> {
    Type getType();
    Function<StatInstance<T>, T> getDefaultSupplier();

    T combine(T base, T other, @Nullable Operation op);
    default T combine(T base, T other) { return combine(base, other, null); }

    default Optional<T> preCombine(T base, T other) {
        if (base == null) return Optional.ofNullable(other);
        if (other == null) return Optional.of(base);
        return null;
    }

    Map<String, Operation> getOperationMap();
}
