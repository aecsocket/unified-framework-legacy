package me.aecsocket.unifiedframework.stat;

public interface StatBased {
    StatData getStats();
}
