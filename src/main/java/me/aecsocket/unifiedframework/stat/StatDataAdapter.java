package me.aecsocket.unifiedframework.stat;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.Type;
import java.util.Map;

public class StatDataAdapter implements UtilJsonAdapter, JsonAdapter<StatData> {
    private Map<String, Stat<?>> stats;

    public StatDataAdapter(Map<String, Stat<?>> stats) {
        this.stats = stats;
    }

    public StatDataAdapter() {}

    public Map<String, Stat<?>> getStats() { return stats; }
    public void setStats(Map<String, Stat<?>> stats) { this.stats = stats; }

    @Override
    public JsonElement serialize(StatData data, Type jType, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        data.forEach((name, inst) -> {
            if (inst.getValue() != null)
                object.add(name, context.serialize(inst.getValue(), inst.getStat().getType()));
        });
        return object;
    }

    @Override
    public StatData deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        if (stats == null) throw new JsonParseException("No stat map was passed to the deserializer");
        JsonObject object = assertObject(json);
        StatData result = new StatData(stats);
        object.entrySet().forEach(entry -> {
            // Find the resolved stat
            String name = entry.getKey();
            if (!stats.containsKey(name)) return;
            Stat<?> stat = stats.get(name);

            // Figure out the operation we're using
            Operation op = null;
            JsonElement elem = entry.getValue();
            if (elem.isJsonArray()) {
                JsonArray arr = elem.getAsJsonArray();
                if (arr.size() > 0 && arr.get(0).isJsonPrimitive() && arr.get(0).getAsJsonPrimitive().isString()) {
                    op = stat.getOperationMap().get(arr.get(0).getAsString());
                    if (op != null) {
                        arr.remove(0);
                        if (arr.size() == 1)
                            elem = arr.get(0);
                        else if (arr.size() == 2 && arr.get(0).isJsonPrimitive() && arr.get(0).getAsJsonPrimitive().isString() && arr.get(0).getAsJsonPrimitive().getAsString().equals("keep_as_array"))
                            arr.remove(0);
                    }
                }
            }

            // Add it to our result
            result.setStat(name, new StatInstance<>(stat, context.deserialize(elem, stat.getType()), op));
        });
        return result;
    }
}
