package me.aecsocket.unifiedframework.stat;

import java.util.*;
import java.util.function.BiConsumer;

public class StatData implements Cloneable {
    private Map<String, StatInstance<?>> stats = new LinkedHashMap<>();

    public StatData(Map<String, Stat<?>> statMap) {
        statMap.forEach((name, stat) -> {
            if (stats.containsKey(name)) return;
            stats.put(name, new StatInstance<>(stat, null, null));
        });
    }

    public StatData() {}

    public Map<String, StatInstance<?>> getStats() { return stats; }

    public StatInstance<?> getStat(String name) { return stats.get(name); }
    public void setStat(String name, StatInstance<?> stat) { stats.put(name, stat); }
    public boolean hasStat(String name) { return stats.containsKey(name); }

    @SuppressWarnings("unchecked")
    public <T> T getValue(String name) { return hasStat(name) ? (T)getStat(name).getValue() : null; }
    public <T> T getValue(String name, Class<T> type) { return getValue(name); }
    @SuppressWarnings("unchecked")
    public <T> StatInstance<T> setValue(String name, T value) {
        if (!stats.containsKey(name)) return null;
        StatInstance<T> stat = (StatInstance<T>)getStat(name);
        stat.setValue(value);
        return stat;
    }
    public <T> StatInstance<T> setValue(String name, T value, Class<T> type) { return setValue(name, value); }

    public Operation getOp(String name) { return getStat(name).getOp(); }
    public StatInstance<?> setOp(String name, Operation op) {
        if (!stats.containsKey(name)) return null;
        StatInstance<?> stat = getStat(name);
        stat.setOp(op);
        return stat;
    }

    @SuppressWarnings("unchecked")
    public <T> T combineValue(String name, T value) {
        if (!stats.containsKey(name)) return null;
        StatInstance<T> stat = (StatInstance<T>)getStat(name);
        if (stat.getValue() != null)
            value = stat.combine(value);
        stat.setValue(value);
        return value;
    }
    @SuppressWarnings("unchecked")
    public <T> T combineValue(String name, T value, Stat<T> stat, Operation op) {
        if (stats.containsKey(name)) {
            StatInstance<T> inst = (StatInstance<T>)stats.get(name);
            value = inst.combine(value);
            inst.setValue(value);
        } else
            stats.put(name, new StatInstance<>(stat, value, op));
        return value;
    }
    @SuppressWarnings("unchecked")
    public <T> T combineValue(String name, StatInstance<T> inst) {
        T value = inst.getRawValue();
        if (stats.containsKey(name)) {
            StatInstance<T> other = (StatInstance<T>)stats.get(name);
            value = other.combine(value, inst.getOp());
            other.setValue(value);
        } else
            stats.put(name, inst.clone());
        return value;
    }

    public StatData combine(StatData other) {
        other.forEach(this::combineValue);
        return this;
    }

    public void forEach(BiConsumer<String, StatInstance<?>> action) { stats.forEach(action); }

    public StatData clone() { try { return (StatData)super.clone(); } catch (CloneNotSupportedException e) { return null; } }
    public StatData copy() {
        StatData clone = clone();
        clone.stats = new LinkedHashMap<>();
        stats.forEach((name, stat) -> clone.stats.put(name, stat.clone()));
        return clone;
    }

    @Override
    public String toString() {
        StringJoiner result = new StringJoiner(", ");
        stats.forEach((name, stat) -> result.add(name + "=" + stat));
        return "StatData{" + result.toString() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatData statData = (StatData)o;
        return stats.equals(statData.stats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stats);
    }
}
