package me.aecsocket.unifiedframework.locale.translation;

import me.aecsocket.unifiedframework.locale.LocaleFormatter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;

/** A map of {@link String} keys linked to {@link String}s. */
public class Translation implements Iterable<Map.Entry<String, String>> {
    private String locale;
    private final Map<String, String> translations;

    public Translation(String locale, Map<String, String> translations) {
        this.locale = locale;
        this.translations = translations;
    }

    public Translation(String locale) {
        this(locale, new HashMap<>());
    }

    /** Gets this instance's locale.
     * @return This instance's locale.
     */
    public String getLocale() { return locale; }

    /** Sets this instance's locale.
     * @param locale This instance's locale.
     */
    public void setLocale(String locale) { this.locale = locale; }

    /** Gets a {@link Map} of all translations.
     * @return A {@link Map} of all translations.
     */
    public Map<String, String> getTranslations() { return translations; }

    /** Gets a string based on a key.
     * @param key The {@link String} key.
     * @return The string.
     */
    public String getString(String key) {
        if (translations.containsKey(key))
            return translations.get(key);
        return null;
    }

    /** Generates a string based on a key using {@link LocaleFormatter}.
     * @param key The {@link String} key.
     * @param args The arguments to format.
     * @return The generated string.
     */
    public String generate(String key, Object... args) {
        if (translations.containsKey(key))
            return LocaleFormatter.format(translations.get(key), this, args);
        else
            return null;
    }

    public boolean has(String key) { return translations.containsKey(key); }

    @NotNull
    @Override
    public Iterator<Map.Entry<String, String>> iterator() { return translations.entrySet().iterator(); }

    public void forEach(BiConsumer<String, String> consumer) { translations.forEach(consumer); }
}
