package me.aecsocket.unifiedframework.locale;

import me.aecsocket.unifiedframework.exception.LocaleException;
import me.aecsocket.unifiedframework.locale.translation.Translation;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.ChatColor;

import java.util.IllegalFormatConversionException;
import java.util.MissingFormatArgumentException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Formats locale-generated strings. */
public final class LocaleFormatter {
    private LocaleFormatter() {}

    /** The color code to use in {@link ChatColor#translateAlternateColorCodes(char, String)}. */
    public static final char ALT_COLOR_CODE = '&';
    /** The regex {@link Pattern} used for {@link LocaleFormatter#formatSubstitutions(String, Translation)}. */
    public static final Pattern SUBSTITUTION = Pattern.compile("#\\{(.+?)}");

    /** Formats a message's colors using {@link ChatColor#translateAlternateColorCodes(char, String)}.
     * @param message The message to format.
     * @return The formatted message.
     */
    public static String formatColors(String message) { return ChatColor.translateAlternateColorCodes(ALT_COLOR_CODE, message); }

    /** Formats a message's arguments using {@link String#format(String, Object...)}.
     * @param message The message to format.
     * @param args The arguments to format.
     * @return The formatted message.
     */
    public static String formatArgs(String message, Object... args) {
        try {
            return String.format(message, args);
        } catch (MissingFormatArgumentException | IllegalFormatConversionException e) {
            throw new LocaleException("Could not format string '" + message + "' (" + FrameworkUtils.arrayToString(args) + ")", e);
        }
    }

    /** Formats a message's substitutions using a {@link Translation}.
     * @param message The message to format.
     * @param substitutions The {@link Translation} substitutions to use.
     * @return The formatted message.
     */
    public static String formatSubstitutions(String message, Translation substitutions) {
        Matcher match = SUBSTITUTION.matcher(message);

        while (match.find()) {
            if (match.groupCount() < 1)
                continue;
            String subtitute = match.group(1);
            String generated = substitutions.getString(subtitute); // doesn't use #generate!
            if (generated == null)
                continue;
            message = match.replaceFirst(generated);
            match = SUBSTITUTION.matcher(message);
        }

        return message;
    }

    /** Fully formats a mesasge.
     * @param message The message to format.
     * @param substitutions The {@link Translation} substitutions to use.
     * @param args The arguments to format.
     * @return The formatted message.
     */
    public static String format(String message, Translation substitutions, Object... args) { return formatArgs(formatColors(formatSubstitutions(message, substitutions)), args); }
}
