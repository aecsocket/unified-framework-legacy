package me.aecsocket.unifiedframework.locale;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import me.aecsocket.unifiedframework.exception.LocaleException;
import me.aecsocket.unifiedframework.locale.translation.Translation;
import me.aecsocket.unifiedframework.resource.index.Index;
import me.aecsocket.unifiedframework.resource.index.Resource;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;

/** Allows creating custom messages for players depending on their locale. */
public class LocaleManager {
    /** The name of the language resources in an {@link Index}. */
    public static final String INDEX_ENTRY = "lang";
    /** The name of the default locale used for fallback. */
    public static final String DEFAULT_LOCALE = "en_us";

    private final Map<Plugin, Map<String, Translation>> translations = new HashMap<>();
    private final Map<Plugin, Translation> fallbackTranslations = new HashMap<>();
    private final Gson gson = new Gson();

    private String fallbackLocale = DEFAULT_LOCALE;
    private String fallbackMessage = "Invalid key %s";

    public String getFallbackLocale() { return fallbackLocale; }
    public void setFallbackLocale(String fallbackLocale) { this.fallbackLocale = fallbackLocale; }

    public String getFallbackMessage() { return fallbackMessage; }
    public void setFallbackMessage(String fallbackMessage) { this.fallbackMessage = fallbackMessage; }

    private Optional<Translation> fallbackTranslation(Plugin plugin) {
        if (fallbackTranslations.containsKey(plugin))
            return Optional.of(fallbackTranslations.get(plugin));
        else {
            if (has(plugin, fallbackLocale)) {
                Translation fallback = get(plugin, fallbackLocale);
                fallbackTranslations.put(plugin, fallback);
                return Optional.of(fallback);
            } else
                return Optional.empty();
        }
    }

    public String fallbackMessage(String key) {
        if (fallbackMessage == null)
            throw new LocaleException("Could not find translation for key '" + key + "'");
        return String.format(fallbackMessage, key);
    }

    public Map<Plugin, Map<String, Translation>> getTranslations() { return translations; }
    public Map<String, Translation> getTranslations(Plugin plugin) { return translations.get(plugin); }
    public Translation get(Plugin plugin, String locale) { return translations.getOrDefault(plugin, Collections.emptyMap()).get(locale); }
    public boolean has(Plugin plugin, String locale) { return translations.containsKey(plugin) && translations.get(plugin).containsKey(locale); }
    public boolean has(Plugin plugin, String locale, String key) { return translations.containsKey(plugin) && translations.get(plugin).containsKey(locale) && translations.get(plugin).get(locale).has(key); }

    public void registerTranslation(Plugin plugin, Translation translation) {
        String locale = translation.getLocale();
        Map<String, Translation> map = translations.computeIfAbsent(plugin, k -> new HashMap<>());
        if (map.containsKey(locale))
            map.get(locale).getTranslations().putAll(translation.getTranslations());
        else
            map.put(locale, translation);
    }
    public List<Translation> registerTranslations(Plugin plugin, Index index) {
        List<Translation> newTranslations = new ArrayList<>();
        if (index != null) {
            if (index.getEntries().containsKey(INDEX_ENTRY)) {
                Map<Resource, String> resources = index.getStringResources(plugin, INDEX_ENTRY);
                for (Map.Entry<Resource, String> entry : resources.entrySet()) {
                    Translation translation;
                    try {
                        translation = gson.fromJson(entry.getValue(), Translation.class);
                    } catch (JsonParseException e) {
                        throw new LocaleException("Failed to load translation " + entry.getKey().getPath() + ": " + e.getMessage());
                    }
                    if (translation != null) {
                        registerTranslation(plugin, translation);
                        newTranslations.add(translation);
                    }
                }
            }
        }
        return newTranslations;
    }
    public List<Translation> registerDataTranslations(Plugin plugin) {
        return walkDataTranslations(plugin, plugin.getDataFolder().toPath().resolve(INDEX_ENTRY));
    }
    private List<Translation> walkDataTranslations(Plugin plugin, Path path) {
        File root = path.toFile();
        List<Translation> newTranslations = new ArrayList<>();
        if (!root.isDirectory())
            return newTranslations;
        for (File file : root.listFiles()) {
            if (file.isDirectory())
                walkDataTranslations(plugin, root.toPath().resolve(file.getName()));
            else {
                String data;
                try {
                    InputStream stream = new FileInputStream(file);
                    data = FrameworkUtils.streamToString(stream);
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }

                Translation translation;
                try {
                    translation = gson.fromJson(data, Translation.class);
                } catch (JsonParseException e) {
                    throw new LocaleException("Failed to load translation " + path.toString() + ": " + e.getMessage());
                }
                if (translation != null) {
                    registerTranslation(plugin, translation);
                    newTranslations.add(translation);
                }
            }
        }
        return newTranslations;
    }

    public void unregisterTranslation(Plugin plugin, Translation translation) {
        String locale = translation.getLocale();
        if (!translations.containsKey(plugin))
            return;
        Map<String, Translation> map = translations.get(plugin);
        if (!map.containsKey(locale))
            return;
        Translation master = map.get(locale);
        translation.forEach((r, s) -> master.getTranslations().remove(r));
    }
    public void unregisterLocale(Plugin plugin, String locale) {
        if (translations.containsKey(plugin))
            translations.get(plugin).remove(locale);
    }

    public void unregisterAll(Plugin plugin) { translations.remove(plugin); }
    public void unregisterAll() { translations.clear(); }

    /** Generates a {@link String} using the registered {@link Translation}s.
     * @param plugin The {@link Plugin}.
     * @param locale The {@link String} locale to use.
     * @param key The key of the string.
     * @param args The {@link LocaleFormatter} arguments.
     * @return The {@link String}.
     */
    public String generate(Plugin plugin, String locale, String key, Object... args) {
        Translation translation;
        if (has(plugin, locale, key))
            translation = get(plugin, locale);
        else if (fallbackTranslation(plugin).isPresent() && fallbackTranslations.containsKey(plugin) && fallbackTranslations.get(plugin).has(key))
            translation = fallbackTranslations.get(plugin);
        else
            return fallbackMessage(key);
        return translation.generate(key, args);
    }

    /** Generates a {@link String} using the registered {@link Translation}s and the fallback locale.
     * @param plugin The {@link Plugin}.
     * @param key The key of the string.
     * @param args The {@link LocaleFormatter} arguments.
     * @return The {@link String}.
     */
    public String generate(Plugin plugin, String key, Object... args) { return generate(plugin, fallbackLocale, key, args); }
}
