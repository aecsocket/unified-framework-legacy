package me.aecsocket.unifiedframework.loop;

/** Used for ticking a {@link Tickable} from a {@link Loop}. */
public class TickContext {
    private Loop loop;
    private boolean removed;

    public TickContext(Loop loop) {
        this.loop = loop;
        this.removed = false;
    }

    public Loop getLoop() { return loop; }

    /** Removes the {@link Tickable} from the current {@link Loop}, stopping it from being ticked. */
    public void removeTickable() { removed = true; }
    public boolean isRemoved() { return removed; }

    /** Properly tick another {@link Tickable} in the same tick.
     * @param other The other {@link Tickable}.
     */
    public void tick(Tickable other) {
        if (other == null)
            return;
        loop.tick(other);
    }

    /** @see Loop#is */
    public boolean is(long period) { return loop.is(period); }
}
