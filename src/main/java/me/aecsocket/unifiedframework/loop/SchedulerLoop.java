package me.aecsocket.unifiedframework.loop;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;

/** A loop using the {@link BukkitScheduler}. This can use Bukkit API. */
public class SchedulerLoop implements Loop {
    public static final int MILLIS_PER_TICK = 50;
    public static final int TICKS_PER_SECOND = 20;

    private final Plugin plugin;
    private long ticks;
    private Set<Tickable> tickables;
    private int task = -1;

    public SchedulerLoop(Plugin plugin, Set<Tickable> tickables) {
        this.plugin = plugin;
        this.tickables = tickables;
    }

    public SchedulerLoop(Plugin plugin) {
        this(plugin, new HashSet<>());
    }

    public Plugin getPlugin() { return plugin; }

    @Override
    public long getTicks() { return ticks; }

    /** Gets a {@link List} of all registered {@link Tickable}s.
     * @return A {@link List} of all registered {@link Tickable}s.
     */
    public Set<Tickable> getTickables() { return tickables; }
    public void setTickables(Set<Tickable> tickables) { this.tickables = tickables; }

    /** Adds a {@link Tickable}.
     * @param tickable The {@link Tickable}.
     */
    public void registerTickable(Tickable tickable) { tickables.add(tickable); }

    /** Removes a {@link Tickable}.
     * @param tickable The {@link Tickable}.
     */
    public void unregisterTickable(Tickable tickable) { tickables.remove(tickable); }

    /** Gets the {@link BukkitScheduler} task of the loop.
     * @return The {@link BukkitScheduler} task of the loop.
     */
    public int getTask() { return task; }

    @Override
    public long getPeriod() { return MILLIS_PER_TICK; }

    public void tick() {
        ++ticks;
        tickables.removeIf(this::tick);
    }

    @Override
    public boolean tick(Tickable tickable) {
        TickContext context = new TickContext(this);
        tickable.tick(context);
        return context.isRemoved();
    }

    @Override
    public void start() {
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this::tick, 0, 1);
    }

    @Override
    public void stop() {
        Bukkit.getScheduler().cancelTask(task);
    }
}
