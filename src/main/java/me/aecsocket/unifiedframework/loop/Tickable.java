package me.aecsocket.unifiedframework.loop;

/** An object which gets ticked by a {@link Loop}. */
public interface Tickable {
    /** Ticks the loop.
     * @param context The {@link TickContext}.
     */
    void tick(TickContext context);
}
