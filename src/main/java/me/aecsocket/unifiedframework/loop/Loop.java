package me.aecsocket.unifiedframework.loop;

import java.util.List;
import java.util.Set;

public interface Loop {
    /** Ticks a specific {@link Tickable} and returns if it is removed or not.
     * @param tickable The {@link Tickable}.
     * @return If this {@link Tickable} is removed or not.
     */
    boolean tick(Tickable tickable);

    /** Ticks all {@link Tickable}s registered to this instance. */
    void tick();

    /** Gets a {@link List} of all registered {@link Tickable}s.
     * @return The {@link List} of all registered {@link Tickable}s.
     */
    Set<Tickable> getTickables();

    /** Registers a {@link Tickable} to be ticked.
     * @param tickable The {@link Tickable} to be ticked.
     */
    default void registerTickable(Tickable tickable) { getTickables().add(tickable); }

    /** Starts the loop. */
    void start();
    /** Stops the loop. */
    void stop();

    /** Gets how many milliseconds it takes for one tick to occur.
     * @return How many milliseconds it takes for one tick to occur.
     */
    long getPeriod();

    /** Gets how many times this has ticked.
     * @return How many times this has ticked.
     */
    long getTicks();

    /** Determines if the current tick is an interval of another amount of ticks.
     * @param period The time period to check.
     * @return If the current tick is an interval of another amount of ticks.
     */
    default boolean is(long period) { return getTicks() % period == 0; }
}
