package me.aecsocket.unifiedframework.loop;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/** A loop using a thread. Do not use Bukkit API with this! */
public class PreciseLoop implements Loop {
    private long ticks;
    private long period;
    private Set<Tickable> tickables;
    private AtomicBoolean running = new AtomicBoolean(false);
    private Thread thread;

    public PreciseLoop(long period, Set<Tickable> tickables) {
        this.period = period;
        this.tickables = tickables;
    }

    public PreciseLoop(long period) { this(period, new HashSet<>()); }

    @Override
    public long getTicks() { return ticks; }
    @Override
    public long getPeriod() { return period; }
    public Thread getThread() { return thread; }

    @Override
    public Set<Tickable> getTickables() { return tickables; }
    public void setTickables(Set<Tickable> tickables) { this.tickables = tickables; }

    @Override
    public boolean tick(Tickable tickable) {
        TickContext context = new TickContext(this);
        tickable.tick(context);
        return context.isRemoved();
    }

    @Override
    public void tick() {
        ++ticks;
        tickables.removeIf(this::tick);
    }

    @Override
    public void start() {
        running.set(true);
        thread = new Thread(() -> {
            while (running.get()) {
                try {
                    tick();
                    synchronized (this) {
                        wait(period);
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            running.set(false);
        });
        thread.start();
    }
    @Override
    public void stop() {
        running.set(false);
        thread.interrupt();
    }
}
