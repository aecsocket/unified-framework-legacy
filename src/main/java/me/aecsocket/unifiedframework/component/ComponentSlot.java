package me.aecsocket.unifiedframework.component;

/** A container for a {@link Component}. */
public interface ComponentSlot {
    /** Gets the {@link Component} stored in this slot.
     * @return The {@link Component}.
     */
    Component get();

    /** Sets the {@link Component} stored in this slot.
     * <br>
     * The component must be compatible with this slot. Check this by using {@link ComponentSlot#isCompatible(Component)} beforehand.
     * @param component The {@link Component}.
     */
    void set(Component component);

    /** Checks if a {@link Component} is compatible with this slot.
     * @param component The {@link Component}.
     * @return If the component is compatible with this slot or not.
     */
    boolean isCompatible(Component component);

    /** Checks if this slot is required to contain a component inside a nested component structure to be complete.
     * @return If this slot is required or not.
     */
    boolean isRequired();

    /** Gets the numerical priority of this slot inside a nested component structure.
     * @return The priority.
     */
    int getPriority();
}
