package me.aecsocket.unifiedframework.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/** A container for multiple {@link ComponentSlot}s. */
public interface ComponentHolder {
    /** The character used to separate ComponentHolders in paths. */
    String COMPONENT_SEPARATOR = ".";
    /** A regex pattern version of {@link ComponentHolder#COMPONENT_SEPARATOR}. */
    String COMPONENT_SEPARATOR_PATTERN = "\\.";

    /** Gets a {@link Map} of {@link ComponentSlot}s linked to their path in this container.
     * @return The {@link Map}.
     */
    Map<String, ? extends ComponentSlot> getSlots();

    /** Gets a {@link ComponentSlot} based on its path, reaching into nested components.
     * @param path The slot path.
     * @return The {@link ComponentSlot} or null if it could not be found.
     */
    default ComponentSlot getSlot(String path) {
        Map<String, ? extends ComponentSlot> slots = getSlots();
        if (slots == null) return null;
        String[] paths = path.split(COMPONENT_SEPARATOR_PATTERN, 2);
        if (paths.length > 1) {
            ComponentSlot slot = slots.get(paths[0]);
            if (slot == null || !(slot.get() instanceof ComponentHolder)) return null;
            return ((ComponentHolder)slot.get()).getSlot(paths[1]);
        } else
            return slots.get(path);
    }

    /** Sets a {@link ComponentSlot} based on its path, reaching into nested components.
     * @param path The slot path.
     * @param toSet The slot to set.
     * @return If the slot could be set or not.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    default boolean setSlot(String path, ComponentSlot toSet) {
        Map<String, ComponentSlot> slots = (Map)getSlots();
        if (slots == null) return false;
        String[] paths = path.split(COMPONENT_SEPARATOR_PATTERN, 2);
        if (paths.length > 1) {
            ComponentSlot slot = slots.get(paths[0]);
            if (slot == null || !(slot.get() instanceof ComponentHolder)) return false;
            return ((ComponentHolder)slot.get()).setSlot(paths[1], toSet);
        } else
            slots.put(path, toSet);
        return true;
    }

    /** Sets a {@link Component} inside a {@link ComponentSlot} based on the slot's path, reaching into nested components.
     * @param path The slot path.
     * @param toSet The slot to set.
     * @return If the component could be set or not.
     */
    default boolean setComponent(String path, Component toSet) {
        ComponentSlot slot = getSlot(path);
        if (slot != null && slot.isCompatible(toSet)) {
            slot.set(toSet);
            return true;
        }
        return false;
    }

    /** Walks the tree of components, with this holder being the root, and runs the {@link Consumer} on all slots.
     * @param path The initial path.
     * @param consumer The {@link Consumer}.
     */
    default void walk(String[] path, Consumer<ComponentWalkData> consumer) {
        Map<String, ? extends ComponentSlot> slots = getSlots();
        if (slots == null) return;
        for (Map.Entry<String, ? extends ComponentSlot> entry : slots.entrySet()) {
            String subPath = entry.getKey();
            ComponentSlot slot = entry.getValue();
            if (consumer != null)
                consumer.accept(new ComponentWalkData(slot, subPath, path));
            if (slot.get() instanceof ComponentHolder) {
                ComponentHolder holder = (ComponentHolder)slot.get();
                if (path == null)
                    holder.walk(new String[]{subPath}, consumer);
                else {
                    String[] newPath = new String[path.length + 1];
                    System.arraycopy(path, 0, newPath, 0, path.length);
                    newPath[newPath.length - 1] = subPath;
                    holder.walk(newPath, consumer);
                }
            }
        }
    }

    /** Walks the tree of components, with this holder being the root, and runs the {@link Consumer} on all slots.
     * @param consumer The {@link Consumer}.
     */
    default void walk(Consumer<ComponentWalkData> consumer) { walk(null, consumer); }

    /** Walks the tree of components, with this holder being the root, and collects all slots into a {@link List}.
     * @return The {@link List} of slots.
     */
    default List<ComponentSlot> getAllSlots() {
        List<ComponentSlot> slots = new ArrayList<>();
        walk(data -> slots.add(data.getSlot()));
        return slots;
    }

    /** Walks the tree of components, with this holder being the root, and collects all components into a {@link List}.
     * @return The {@link List} of components.
     */
    default List<Component> getAllComponents() {
        List<Component> components = new ArrayList<>();
        walk(data -> {
            if (data.getComponent() != null)
                components.add(data.getComponent());
        });
        return components;
    }
}
