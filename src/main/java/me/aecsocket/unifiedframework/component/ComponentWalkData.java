package me.aecsocket.unifiedframework.component;

public class ComponentWalkData {
    private final ComponentSlot slot;
    private final Component component;
    private final String slotName;
    private final String[] parents;

    public ComponentWalkData(ComponentSlot slot, String slotName, String[] parents) {
        this.slot = slot;
        component = slot == null ? null : slot.get();
        this.slotName = slotName;
        this.parents = parents;
    }

    public ComponentSlot getSlot() { return slot; }
    public Component getComponent() { return component; }
    public String getSlotName() { return slotName; }
    public String[] getParents() { return parents; }
    public int getDepth() { return parents == null ? 0 : parents.length; }
}
