package me.aecsocket.unifiedframework.component;

/** A component which can be placed inside a {@link ComponentSlot}. */
public interface Component {}
