package me.aecsocket.unifiedframework.registry;

import me.aecsocket.unifiedframework.exception.ResolveException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ref<T extends Identifiable> implements Identifiable {
    private T object;
    private String type;
    private String id;

    public Ref(T object) {
        this.object = object;
        update();
    }

    public Ref(String type, String id) {
        this.type = type;
        this.id = id;
    }

    private void update() {
        if (object == null)
            return;
        type = object.getType();
        id = object.getId();
    }

    public T get() { return object; }
    public void set(T object) { this.object = object; update(); }

    @Override
    public String getType() { return type; }
    public void setType(String type) { this.type = type; object = null; }

    @Override
    public String getId() { return id; }
    public void setId(String id) { this.id = id; object = null; }

    public boolean isResolved() { return object != null; }

    public void resolve(Registry registry) {
        object = registry.get(type, id);
        if (object == null)
            throw new ResolveException("Object " + id + " does not exist");
    }

    @Override
    public String toString() {
        return "Ref{" + type + ":" + id + (isResolved() ? "*" : "") + "}";
    }

    public static <T extends Identifiable> List<Ref<T>> to(List<T> list, Registry registry) {
        List<Ref<T>> result = new ArrayList<>();
        list.forEach(id -> result.add(registry.getRef(id)));
        return result;
    }

    public static <T extends Identifiable, V> Map<Ref<T>, V> to(Map<T, V> map, Registry registry) {
        Map<Ref<T>, V> result = new HashMap<>();
        map.forEach((id, v) -> result.put(registry.getRef(id), v));
        return result;
    }

    public static <T extends Identifiable> List<T> from(List<Ref<T>> list) {
        List<T> result = new ArrayList<>();
        list.forEach(ref -> result.add(ref.get()));
        return result;
    }

    public static <T extends Identifiable, V> Map<T, V> from(Map<Ref<T>, V> map) {
        Map<T, V> result = new HashMap<>();
        map.forEach((ref, v) -> result.put(ref.get(), v));
        return result;
    }
}
