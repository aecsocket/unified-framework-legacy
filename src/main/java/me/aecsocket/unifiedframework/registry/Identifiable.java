package me.aecsocket.unifiedframework.registry;

public interface Identifiable {
    String getType();
    String getId();
    default void resolve(Registry registry) {}
    default void reload(Identifiable other) {}

    default void register(Registry registry) { registry.register(this); }
    default void unregister(Registry registry) { registry.unregister(this); }
}
