package me.aecsocket.unifiedframework.registry;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private Map<String, Map<String, Identifiable>> registry = new HashMap<>();
    private Map<String, Map<String, Ref<?>>> refs = new HashMap<>();

    // Registry
    public Map<String, Map<String, Identifiable>> get() { return new HashMap<>(registry); }
    public Map<String, Identifiable> get(String type) { return registry.containsKey(type) ? new HashMap<>(registry.get(type)) : null; }
    @SuppressWarnings("unchecked")
    public <T> T get(String type, String id) { return registry.containsKey(type) ? (T)registry.get(type).get(id) : null; }
    public <T> T getOrDefault(String type, String id, String defaultId) { return get(type, has(type, id) ? id : defaultId); }

    public boolean has(String type) { return registry.containsKey(type); }
    public boolean has(String type, String id) { return registry.containsKey(type) && registry.get(type).containsKey(id); }
    public boolean has(String type, String id, Class<? extends Identifiable> clazz) { return has(type, id) && clazz.isAssignableFrom(get(type, id).getClass()); }

    public void register(Identifiable id) { registry.computeIfAbsent(id.getType(), type -> new HashMap<>()).put(id.getId(), id); }
    @SuppressWarnings("unchecked")
    public <T extends Identifiable> T unregister(String type, String id) { return registry.containsKey(type) ? (T)registry.remove(id) : null; }
    public void unregister(Identifiable id) { unregister(id.getType(), id.getId()); }
    public void clear() {
        registry.clear();
        refs.forEach((type, map) -> map.forEach((id, ref) -> ref.set(null)));
    }

    public void reload(Map<String, Map<String, Identifiable>> other) {
        other.forEach((type, map) -> map.forEach((id, obj) -> {
            Identifiable otherObj = registry.containsKey(type) ? registry.get(type).get(id) : null;
            if (otherObj != null)
                obj.reload(otherObj);
        }));
        registry = new HashMap<>(other);
    }

    // Resolver
    public void resolve(Identifiable id) { id.resolve(this); }
    public void resolve() { registry.forEach((type, map) -> map.forEach((id, obj) -> resolve(obj))); }

    // Refs
    public Map<String, Map<String, Ref<?>>> getRefs() { return new HashMap<>(refs); }
    @SuppressWarnings("unchecked")
    public <T extends Identifiable> Ref<T> getRef(String type, String id) {
        if (refs.containsKey(type) && refs.get(type).containsKey(id))
            return (Ref<T>)refs.get(type).get(id);
        Ref<T> ref = new Ref<>(type, id);
        refs.computeIfAbsent(type, t -> new HashMap<>()).put(id, ref);
        return ref;
    }
    public <T extends Identifiable> Ref<T> resolveRef(String type, String id) {
        Ref<T> ref = getRef(type, id);
        ref.resolve(this);
        return ref;
    }
    public <T extends Identifiable> Ref<T> getRef(T object) { return resolveRef(object.getType(), object.getId()); }
}
