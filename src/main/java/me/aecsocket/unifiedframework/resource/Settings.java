package me.aecsocket.unifiedframework.resource;

import com.google.gson.*;
import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class Settings {
    public static final String FILE = "settings.json";
    public static final String SECTION_SEPARATOR = "\\.";

    private JsonObject values;
    private final Map<String, Object> cache = new HashMap<>();
    private Gson gson;

    private Settings(Gson gson, JsonObject values) {
        this.gson = gson;
        this.values = values;
    }

    public Settings(Gson gson) {
        this(gson, new JsonObject());
    }

    public Gson getGson() { return gson; }
    public void setGson(Gson gson) { this.gson = gson; }

    public JsonObject getValues() { return values; }

    public Map<String, Object> getCache() { return new HashMap<>(cache); }
    public void clearCache() { cache.clear(); }
    public void addToCache(String path, Object value) { cache.put(path, value); }
    @SuppressWarnings("unchecked")
    public <T> T getFromCache(String path) { return (T)cache.get(path); }

    private <T> T result(String path, JsonElement elem, Type type) {
        try {
            T obj = gson.fromJson(elem, type);
            cache.put(path, obj);
            return obj;
        } catch (JsonParseException e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String path, Type type) {
        if (cache.containsKey(path))
            return (T)cache.get(path);

        String[] paths = path.split(SECTION_SEPARATOR);
        String last = paths[paths.length - 1];
        JsonObject obj = values;
        for (String sub : paths) {
            if (obj.has(sub)) {
                JsonElement elem = obj.get(sub);
                if (sub.equals(last))
                    return result(path, elem, type);

                if (elem.isJsonArray()) {
                    JsonArray array = elem.getAsJsonArray();
                    obj = new JsonObject();
                    for (int i = 0; i < array.size(); i++)
                        obj.add(Integer.toString(i), array.get(i));
                } else if (elem.isJsonObject())
                    obj = elem.getAsJsonObject();
                else
                    return result(path, elem, type);
            } else
                break;
        }
        return null;
    }

    public static Settings loadFrom(Plugin plugin, Gson gson, String path) throws IOException, JsonParseException {
        return new Settings(
                gson,
                gson.fromJson(
                        Files.readString(plugin.getDataFolder().toPath().resolve(path)), JsonObject.class
                )
        );
    }

    public static Settings loadFrom(Plugin plugin, Gson gson) throws IOException, JsonParseException {
        return loadFrom(plugin, gson, FILE);
    }
}
