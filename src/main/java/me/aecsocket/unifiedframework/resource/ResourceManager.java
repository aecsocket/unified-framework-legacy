package me.aecsocket.unifiedframework.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.aecsocket.unifiedframework.exception.ResourceException;
import me.aecsocket.unifiedframework.resource.index.Index;
import me.aecsocket.unifiedframework.resource.index.IndexEntry;
import me.aecsocket.unifiedframework.resource.index.Resource;
import me.aecsocket.unifiedframework.resource.index.adapter.IndexAdapter;
import me.aecsocket.unifiedframework.resource.index.adapter.IndexEntryAdapter;
import me.aecsocket.unifiedframework.resource.index.adapter.ResourceAdapter;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.plugin.Plugin;

import java.io.*;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/** Reads/writes to/from plugin data files and resources. */
public class ResourceManager {
    private Gson gson;

    public ResourceManager(Gson gson) {
        this.gson = gson;
    }
    public ResourceManager() {
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(Resource.class, new ResourceAdapter())
                .registerTypeAdapter(IndexEntry.class, new IndexEntryAdapter())
                .registerTypeAdapter(Index.class, new IndexAdapter(this))
                .create();
    }

    public Gson getGson() { return gson; }
    public void setGson(Gson gson) { this.gson = gson; }

    private String getString(InputStream stream) {
        if (stream == null)
            return null;

        String string = FrameworkUtils.streamToString(stream);
        try {
            stream.close();
        } catch (IOException e) {
            throw new ResourceException(e);
        }
        return string;
    }

    /** Gets an {@link InputStream} resource from a {@link Plugin}.
     * @param plugin The {@link Plugin}.
     * @param path The file path to the resource.
     * @return The resource {@link InputStream}.
     */
    public InputStream getResource(Plugin plugin, String path) { return plugin.getResource(path); }

    /** Gets a {@link String} resource from a {@link Plugin}.
     * @param plugin The {@link Plugin}.
     * @param path The file path to the resource.
     * @return The resource {@link String}.
     */
    public String getStringResource(Plugin plugin, String path) { return getString(getResource(plugin, path)); }

    /** Follows paths in a {@link Plugin}'s data folder.
     * @param plugin The {@link Plugin}.
     * @param path The file path to the root.
     * @return The {@link List} of {@link Path}s.
     */
    public List<Path> getPaths(Plugin plugin, String path) {
        List<Path> result = new ArrayList<>();
        try {
            Files.walk(Paths.get(plugin.getDataFolder().getAbsolutePath(), path), FileVisitOption.FOLLOW_LINKS)
                    .forEach(result::add);
        } catch (IOException e) {
            throw new ResourceException(e);
        }
        return result;
    }

    /** Gets {@link InputStream} data from a file in the {@link Plugin}'s data folder ({@link Plugin#getDataFolder()}).
     * @param plugin The {@link Plugin}.
     * @param path The file path to the data file.
     * @return The data {@link InputStream}.
     */
    public InputStream getData(Plugin plugin, String path) {
        File file = new File(plugin.getDataFolder(), path);
        if (file.exists()) {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException e) {
                throw new ResourceException(e);
            }
        }
        return null;
    }

    /** Gets {@link String} data from a file in the {@link Plugin}'s data folder ({@link Plugin#getDataFolder()}).
     * @param plugin The {@link Plugin}.
     * @param path The file path to the data file.
     * @return The data {@link String}.
     */
    public String getStringData(Plugin plugin, String path) { return getString(getData(plugin, path)); }

    /** Saves {@link String} data to a file in the {@link Plugin}'s data folder ({@link Plugin#getDataFolder()}).
     * @param plugin The {@link Plugin}.
     * @param path The file path to the data file.
     * @param append If the writer should append data or not.
     * @param overwrite If the writer should overwrite the file or not.
     * @param data The data {@link String}.
     */
    public void saveData(Plugin plugin, String path, boolean append, boolean overwrite, String data) {
        File file = new File(plugin.getDataFolder().getAbsoluteFile(), path);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                if (!file.createNewFile())
                    return;
            } else if (!overwrite)
                return;

            FileWriter writer = new FileWriter(file, append);
            writer.write(data);
            writer.close();
        } catch (IOException e) {
            throw new ResourceException(e);
        }
    }

    /** Gets a {@link Plugin}'s {@link Index}.
     * @param plugin The {@link Plugin}.
     * @return The {@link Index}.
     */
    public Index getIndex(Plugin plugin) {
        String json = getStringResource(plugin, Index.PATH);
        return gson.fromJson(json, Index.class);
    }
}
