package me.aecsocket.unifiedframework.resource.index.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.resource.index.IndexEntry;
import me.aecsocket.unifiedframework.resource.index.Resource;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class IndexEntryAdapter implements JsonAdapter<IndexEntry> {
    @Override
    public JsonElement serialize(IndexEntry entry, Type jType, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        for (Resource resource : entry.getResources())
            array.add(context.serialize(resource));
        return array;
    }

    @Override
    public IndexEntry deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = JsonUtils.assertArray(json);
        List<Resource> resources = new ArrayList<>();
        for (JsonElement element : array)
            resources.add(context.deserialize(element, Resource.class));
        return new IndexEntry(resources);
    }
}
