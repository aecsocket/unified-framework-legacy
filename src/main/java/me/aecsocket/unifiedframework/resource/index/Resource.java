package me.aecsocket.unifiedframework.resource.index;

import me.aecsocket.unifiedframework.resource.ResourceManager;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.plugin.Plugin;

import java.io.InputStream;

/** A resource of an {@link IndexEntry}, has a path and also caches its {@link InputStream} and {@link String} data. */
public class Resource {
    private String path;
    private InputStream stream;
    private String data;

    public Resource(String path, InputStream stream) {
        this.path = path;
        this.stream = stream;
        this.data = stream == null ? null : FrameworkUtils.streamToString(stream);
    }

    public Resource(String path) {
        this(path, null);
    }

    /** Gets the file path.
     * @return The file path.
     */
    public String getPath() { return path; }

    /** Sets the file path.
     * @param path The file path.
     */
    public void setPath(String path) { this.path = path; }

    /** Gets the {@link InputStream}.
     * @return The {@link InputStream}.
     */
    public InputStream getStream() { return stream; }

    /** Sets the {@link InputStream} and resets the data.
     * @param stream The {@link InputStream}.
     */
    public void setStream(InputStream stream) {
        if (this.stream != stream) {
            this.stream = stream;
            this.data = null;
        }
    }

    /** Gets the {@link String} form of the {@link InputStream}.
     * @return The {@link String} form of the {@link InputStream}.
     */
    public String getData() { return data; }

    /** Opens this {@link InputStream} and sets the data.
     * @param manager The {@link ResourceManager} to use.
     * @param plugin The {@link Plugin}.
     */
    public void open(ResourceManager manager, Plugin plugin) {
        if (data == null) {
            stream = manager.getResource(plugin, path);
            if (stream != null)
                data = FrameworkUtils.streamToString(stream);
        }
    }

    @Override
    public String toString() { return path; }
}
