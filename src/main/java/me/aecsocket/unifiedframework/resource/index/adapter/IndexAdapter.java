package me.aecsocket.unifiedframework.resource.index.adapter;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import me.aecsocket.unifiedframework.resource.ResourceManager;
import me.aecsocket.unifiedframework.resource.index.Index;
import me.aecsocket.unifiedframework.resource.index.IndexEntry;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;

import java.lang.reflect.Type;
import java.util.Map;

public class IndexAdapter implements JsonAdapter<Index> {
    private final ResourceManager manager;

    public IndexAdapter(ResourceManager manager) {
        this.manager = manager;
    }

    public ResourceManager getManager() { return manager; }

    @Override
    public JsonElement serialize(Index index, Type jType, JsonSerializationContext context) {
        return context.serialize(index.getEntries());
    }

    @Override
    public Index deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = JsonUtils.assertObject(json);
        return new Index(manager, context.deserialize(object, new TypeToken<Map<String, IndexEntry>>(){}.getType()));
    }
}
