package me.aecsocket.unifiedframework.resource.index;

import me.aecsocket.unifiedframework.resource.ResourceManager;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** An entry of an {@link Index}, stores {@link Resource}s. */
public class IndexEntry {
    private final List<Resource> resources;

    public IndexEntry(List<Resource> resources) {
        this.resources = resources;
    }

    public IndexEntry(Collection<Resource> resources) {
        this(new ArrayList<>(resources));
    }

    public IndexEntry() {
        this(new ArrayList<>());
    }

    /** Gets a {@link List} of all {@link Resource}s.
     * @return  The {@link List} of all {@link Resource}s.
     */
    public List<Resource> getResources() { return resources; }

    /** Creates an {@link IndexEntry} from a {@link Collection} of {@link String} paths.
     * @param paths The {@link Collection} of {@link String} paths.
     * @return The {@link IndexEntry}.
     */
    public static IndexEntry createFromPaths(Collection<String> paths) {
        List<Resource> resources = new ArrayList<>();
        for (String path : paths)
            resources.add(new Resource(path));
        return new IndexEntry(resources);
    }

    /** Opens all {@link Resource}s.
     * @param manager The {@link ResourceManager} to use.
     * @param plugin The {@link Plugin}.
     */
    public void open(ResourceManager manager, Plugin plugin) { resources.forEach(r -> r.open(manager, plugin)); }

    @Override
    public String toString() { return resources.toString(); }
}
