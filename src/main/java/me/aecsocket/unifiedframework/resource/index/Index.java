package me.aecsocket.unifiedframework.resource.index;

import me.aecsocket.unifiedframework.resource.ResourceManager;
import org.bukkit.plugin.Plugin;

import java.io.InputStream;
import java.util.*;

/** A class for plugins to keep links to {@link Resource}s. */
public class Index {
    /** The index's default resource file path in a plugin. */
    public static final String PATH = "index.json";

    private final ResourceManager manager;
    private final Map<String, IndexEntry> entries;

    public Index(ResourceManager manager, Map<String, IndexEntry> entries) {
        this.manager = manager;
        this.entries = entries;
    }

    public Index(ResourceManager manager) {
        this(manager, new HashMap<>());
    }

    public ResourceManager getManager() { return manager; }

    /** Gets a {@link Map} of {@link String} names to {@link IndexEntry} entries.
     * @return A {@link Map} of {@link String} names to {@link IndexEntry} entries.
     */
    public Map<String, IndexEntry> getEntries() { return entries; }

    /** Gets if an {@link IndexEntry} exists.
     * @param name The name of the entry.
     * @return If the {@link IndexEntry} exists.
     */
    public boolean has(String name) { return entries.containsKey(name); }

    /** Gets an {@link IndexEntry} from the map.
     * @param name The name of the entry.
     * @return The {@link IndexEntry}.
     */
    public IndexEntry getEntry(String name) {
        if (entries.containsKey(name))
            return entries.get(name);
        return null;
    }

    /** Gets a {@link Map} of {@link Resource}s linked to their {@link InputStream} resources of a specific {@link IndexEntry} from its name from a {@link Plugin}.
     * @param manager The {@link ResourceManager} to use.
     * @param plugin The {@link Plugin}.
     * @param name The name of the entry.
     * @return The {@link Map} of {@link InputStream} resources.
     */
    public Map<Resource, InputStream> getResources(ResourceManager manager, Plugin plugin, String name) {
        if (entries.containsKey(name)) {
            IndexEntry entry = entries.get(name);
            Map<Resource, InputStream> streams = new HashMap<>();
            for (Resource resource : entry.getResources()) {
                resource.open(manager, plugin);
                streams.put(resource, resource.getStream());
            }
            return streams;
        }
        return null;
    }

    /** Gets a {@link Map} of {@link Resource}s linked to their {@link String} data of a specific {@link IndexEntry} from its name from a {@link Plugin}.
     * @param plugin The {@link Plugin}.
     * @param name The name of the entry.
     * @return The {@link Map} of {@link String} resources.
     */
    public Map<Resource, String> getStringResources(Plugin plugin, String name) {
        if (entries.containsKey(name)) {
            IndexEntry entry = entries.get(name);
            Map<Resource, String> data = new HashMap<>();
            for (Resource resource : entry.getResources()) {
                resource.open(manager, plugin);
                data.put(resource, resource.getData());
            }
            return data;
        }
        return null;
    }

    /** Opens all {@link IndexEntry} entries.
     * @param plugin The {@link Plugin}.
     */
    public void open(Plugin plugin) {
        entries.forEach((r, s) -> s.open(manager, plugin));
    }

    /** Saves all IndexEntries under the name in the plugin's data folder.
     * @param plugin The {@link Plugin}.
     * @param name The {@link IndexEntry} name.
     */
    public void saveAsData(Plugin plugin, String name) {
        Map<Resource, String> resources = getStringResources(plugin, name);
        resources.forEach((r, s) -> manager.saveData(plugin, r.getPath(), false, false, s));
    }

    @Override
    public String toString() { return entries.toString(); }
}
