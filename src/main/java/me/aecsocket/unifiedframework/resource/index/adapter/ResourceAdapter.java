package me.aecsocket.unifiedframework.resource.index.adapter;

import com.google.gson.*;
import me.aecsocket.unifiedframework.resource.index.Resource;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;

import java.lang.reflect.Type;

public class ResourceAdapter implements JsonAdapter<Resource> {
    @Override
    public JsonElement serialize(Resource resource, Type jType, JsonSerializationContext context) {
        return new JsonPrimitive(resource.getPath());
    }

    @Override
    public Resource deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonPrimitive primitive = JsonUtils.assertPrimitive(json);
        return new Resource(primitive.getAsString());
    }
}
