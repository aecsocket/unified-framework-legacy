package me.aecsocket.unifiedframework.item.core;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

/** A custom item. */
public interface Item {
    /** Gets the type of this item. This must match with the corresponding {@link ItemAdapter#getType()}.
     * @return The type of this item.
     */
    String getItemType();

    /** Gets the {@link ItemStack} representation of this item.
     * <p>
     * There should be enough info in this to convert it back to this instance using
     * {@link ItemAdapter#deserialize(ItemStack)}. Consider using the {@link org.bukkit.persistence.PersistentDataContainer}.
     * @param player The {@link Player} to give this item to. Can be null.
     * @param amount The stack size of the item.
     * @return The {@link ItemStack} representation of this item.
     */
    ItemStack create(@Nullable Player player, int amount);
}
