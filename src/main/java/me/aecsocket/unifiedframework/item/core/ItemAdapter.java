package me.aecsocket.unifiedframework.item.core;

import org.bukkit.inventory.ItemStack;

public interface ItemAdapter<T extends Item> {
    /** Gets the type of this item adapter. This must match with the corresponding {@link Item#getItemType()}.
     * @return The type of this item adapter.
     */
    String getType();

    /** Converts an {@link ItemStack} of this type to {@link T}.
     * @param item The {@link ItemStack}.
     * @return {@link T}.
     */
    T deserialize(ItemStack item);
}
