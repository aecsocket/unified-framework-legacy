package me.aecsocket.unifiedframework.item;

import me.aecsocket.unifiedframework.item.core.Item;
import me.aecsocket.unifiedframework.item.core.ItemAdapter;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;

/** Allows creating custom items with custom metadata. */
public class ItemManager {
    private static NamespacedKey TYPE = null;

    private Map<String, ItemAdapter<?>> itemAdapters;

    public ItemManager(Plugin plugin) {
        this.itemAdapters = new HashMap<>();
        TYPE = new NamespacedKey(plugin, "type");
    }

    /** Gets the item type {@link NamespacedKey}.
     * @return The item type {@link NamespacedKey}.
     */
    public NamespacedKey getTypeKey() { return TYPE; }

    /** Gets a {@link Map} of {@link ItemAdapter}s paired with their {@link String} types.
     * @return A {@link Map} of {@link ItemAdapter}s paired with their {@link String} types.
     */
    public Map<String, ItemAdapter<?>> getItemAdapters() { return itemAdapters; }

    /** Adds an {@link ItemAdapter}.
     * @param itemAdapter The {@link ItemAdapter}.
     */
    public void registerItemAdapter(ItemAdapter<?> itemAdapter) { itemAdapters.put(itemAdapter.getType(), itemAdapter); }

    /** Removes an {@link ItemAdapter}.
     * @param itemAdapter The {@link ItemAdapter}.
     */
    public void unregisterItemAdapter(ItemAdapter<?> itemAdapter) { itemAdapters.remove(itemAdapter.getType()); }

    /** Gets an {@link Item} from an {@link ItemStack} if possible using {@link ItemAdapter#deserialize(ItemStack)}.
     * @param item The {@link ItemStack}.
     * @return The {@link Item} or null if there is no {@link Item} representation.
     */
    public Item getItem(ItemStack item) {
        if (item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();
            PersistentDataContainer data = meta.getPersistentDataContainer();
            if (data.has(TYPE, PersistentDataType.STRING)) {
                String type = data.get(TYPE, PersistentDataType.STRING);
                if (itemAdapters.containsKey(type))
                    return itemAdapters.get(type).deserialize(item);
            }
        }
        return null;
    }
}
