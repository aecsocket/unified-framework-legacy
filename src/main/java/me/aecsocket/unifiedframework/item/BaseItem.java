package me.aecsocket.unifiedframework.item;

import me.aecsocket.unifiedframework.item.core.Item;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.Nullable;

/** A helper wrapper around {@link Item}. */
public abstract class BaseItem implements Item {
    /** Gets the {@link ItemStack} before the type key is added.
     * @param player The {@link Player} to generate the {@link ItemStack} for.
     * @return The {@link ItemStack}.
     */
    public abstract ItemStack createBase(Player player);

    protected abstract ItemManager getItemManager();

    @Override
    public ItemStack create(@Nullable Player player, int amount) {
        ItemStack item = createBase(player);
        ItemMeta meta = item.getItemMeta();
        if (meta == null)
            meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        PersistentDataContainer data = meta.getPersistentDataContainer();

        data.set(getItemManager().getTypeKey(), PersistentDataType.STRING, getItemType());

        item.setItemMeta(meta);
        return item;
    }
}
