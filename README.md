# UnifiedFramework

A combination of multiple frameworks to aid plugin developers.

---

This library has multiple frameworks inside, such as ones for read/writing resources and language/locale features, to
help people develop plugins with less boilerplate.
**Important note:** This code is no longer a standalone plugin. If you want to use this, you must shade it into your own plugin JAR.

## For developers

No documentation here yet, but the [javadocs](src/main/javadoc) have a lot of useful information. Pick a topic and start reading.
Please note that a lot of APIs here are for use in my own plugins, so don't expect everything to stay stable.

## Download

Currently there is no JAR download available. You must compile it yourself.
With Maven, run `mvn clean install` in the root folder. Then you can use the classes in `me.aecsocket.unifiedframework`.
